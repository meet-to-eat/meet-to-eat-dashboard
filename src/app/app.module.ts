import { RoleProviderService } from './services/role-provider.service';
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
  NbThemeModule,
  NbLayoutDirection,
} from '@nebular/theme';
import { IsAuthenticatedService } from './services/is-authenticated.service';
import { NbAuthJWTInterceptor, NB_AUTH_TOKEN_INTERCEPTOR_FILTER } from '@nebular/auth';
import { PagingInterceptorService } from './services/paging-interceptor.service';
import { LanguageInterceptorService } from './services/language-interceptor';

import { JwtModule } from '@auth0/angular-jwt';
import { ApiKeyInterceptorService } from './services/apiKey-interceptor.service';


@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),

    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    NbThemeModule.forRoot({ name: 'corporate' }, null, null, NbLayoutDirection.LTR),

    NbDatepickerModule.forRoot(),
    // JwtModule.forRoot({
    //   config: {
    //     whitelistedDomains: [/^null$/],
    //     // tokenGetter: () => {
    //     //   return localStorage.getItem("access_token");
    //     // }
    //   }
    // }),
    

  ],
  providers: [
    IsAuthenticatedService,
    // ApiKeyInterceptorService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiKeyInterceptorService,
      multi: true
    },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: LanguageInterceptorService,
    //   multi: true
    // },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: NbAuthJWTInterceptor, // will add the saved token for each request
    //   multi: true
    // },
    // {
    //   provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER,
    //   useValue: function () { return false; }, //returning true will disable the NbAuthJWTInterceptor
    // },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: PagingInterceptorService,
    //   multi: true
    // },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
