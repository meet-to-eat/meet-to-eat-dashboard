import { Injectable } from '@angular/core';
import * as arabicTranslation from './translation-ar.json';
import * as englishTranslation from './translation-en.json';

@Injectable({
  providedIn: 'root'
})
export class TranslateService {

  constructor() { }

  translate(key, language){
    if(language == 'ar') {
      return (arabicTranslation as any).default[key] || key;
    }
    return (englishTranslation as any).default[key] || key;
  }
}
