import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from './translate.service';
import { NbLayoutDirectionService } from '@nebular/theme';

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(
    private translateService: TranslateService,
    private directionService: NbLayoutDirectionService
  ) { }


  transform(value: any, ...args: any[]): any {
    let currentLanguage = this.directionService.getDirection() == 'ltr' ? 'en' : 'ar';
    
    return this.translateService.translate(value, currentLanguage);
  }

}
