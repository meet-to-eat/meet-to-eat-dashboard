import { filter } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { FormControl } from '@angular/forms';
import { DefaultFilter } from 'ng2-smart-table';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'ngx-date-filter',
  templateUrl: './date-filter.component.html',
  styleUrls: ['./date-filter.component.scss']
})
export class DateFilterComponent extends DefaultFilter implements OnInit, OnChanges {

  updateAfter: FormControl;
  updateBefore: FormControl;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.updateAfter = new FormControl(null);
    this.updateBefore = new FormControl(null);

    this.updateAfter.valueChanges.pipe(filter((value) => value != null))
      .subscribe(
        (value) => {
          let id = this.column.id;
          if (this.column.filter.config) this.column.id = this.column.filter.config.date_1 || 'UpdatedAfter';
          else this.column.id = 'UpdatedAfter';
          this.query = new Date(value).toISOString();
          this.setFilter();
          this.column.id = id;
        },
        (error) => console.error(error)
      );

    this.updateBefore.valueChanges.pipe(filter((value) => value != null))
      .subscribe(
        (value) => {
          let id = this.column.id;
          if (this.column.filter.config) this.column.id = this.column.filter.config.date_2 || 'UpdatedBefore';
          else this.column.id = 'UpdatedBefore';
          this.query = new Date(value).toISOString();
          this.setFilter();
          this.column.id = id;
        },
        (error) => console.error(error)
      );
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  resetFilters() {
    let id = this.column.id;

    this.updateAfter.setValue(null);
    this.updateBefore.setValue(null);
    
    this.column.id = this.column.filter.config.date_1;
    this.query = null;
    this.setFilter();

    this.column.id = this.column.filter.config.date_2;
    this.query = null;
    this.setFilter();

    this.column.id = id;

  }
}
