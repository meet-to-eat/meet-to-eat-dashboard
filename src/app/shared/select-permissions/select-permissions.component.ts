import { DashboardTableComponent } from './../dashboard-table/dashboard-table.component';
import { NbDialogRef } from '@nebular/theme';
import { environment } from './../../../environments/environment';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ngx-select-permissions',
  templateUrl: './select-permissions.component.html',
  styleUrls: ['./select-permissions.component.scss']
})
export class SelectPermissionsComponent implements OnInit {

  @ViewChild('dashboardTable', { static: false }) dashboardTable: DashboardTableComponent;
  endpoint: string;
  actions: any[];
  columns: any = undefined;
  
  constructor(
    private nbDialogRef: NbDialogRef<SelectPermissionsComponent>,
    ) { 
      this.endpoint = `${environment.API_URL}/role/create`;   
       this.initColumns();
  }

  ngOnInit(): void {
  }

  initColumns() {
    this.columns = {
      name: { title: 'Role', type: 'string', filter: false, sort: false },
    }
  }

  onSubmit($event){
    console.log($event);
    this.nbDialogRef.close({name : $event.length, data: $event});
  }

}
