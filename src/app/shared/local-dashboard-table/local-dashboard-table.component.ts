import { Component, OnInit, Input, Output } from "@angular/core";
import { Observable, BehaviorSubject } from "rxjs";
import { EventEmitter } from "@angular/core";
import { LocalDataSource, ServerDataSource } from "ng2-smart-table";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TextBoxFormItem } from '../dynamic-form/form-items/text-box';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

@Component({
  selector: "local-dashboard-table",
  templateUrl: "./local-dashboard-table.component.html",
  styleUrls: ["./local-dashboard-table.component.scss"],
})
export class LocalDashboardTableComponent implements OnInit {
  @Input() endpoint: string;
  @Input() actions: {};
  @Input() columns: {};
  @Input() dataKey = "data";
  @Input() filters: {field : string, type: 'text' | 'number', title: string, sort: boolean}[] = []
  
  
  @Output() onAdd = new EventEmitter();
  @Output() onEdit = new EventEmitter();
  @Output() onDelete = new EventEmitter();
  @Output() onView = new EventEmitter();


  filtersForm : FormGroup;
  sort = '-fullName'

  data: BehaviorSubject<[]> = new BehaviorSubject([]);
  settings = {
    actions: {
      custom: [
        {
          name: "view",
          title: '<i class="nb-info"></i>',
        },
        {
          name: "edit",
          title: '<i class="nb-edit"></i>',
        },
        {
          name: "delete",
          title: '<i class="nb-trash"></i>',
        },
      ],
      add: true,
      edit: false,
      delete: false,
    },
    mode: "external",
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {},
  };

  source: LocalDataSource;

  constructor(private http: HttpClient,private formBuilder : FormBuilder) {}

  ngOnInit(): void {
    Object.keys(this.settings.columns).forEach((value)=>{
      this.settings.columns[value].filter = false;
      this.settings.columns[value].sort = false;
    })

    this.settings.actions = {
      ...this.settings.actions,
      ...this.actions,
    };

    this.settings.columns = {
      ...this.columns,
    };

    this.data.subscribe((data) => {
      this.source = new LocalDataSource(data);
    });

    this.buildFiltersForm();
    this.getData();


  }

  buildFiltersForm() {
    let formFields = {}
      this.filters.forEach(( value , index)=>{
        switch(value.type)  {
          case  'text':
            formFields[value.field] = ['']
        }
      })
    
    this.filtersForm = this.formBuilder.group({...formFields})
  }
  
  getSortClass(field : string){
    // console.log(field)
    // console.log(this.sort)
    // console.log(this.sort == field)
    if(field == this.sort)
      return 'up'
    else if(this.sort.replace('-','') == field)
      return 'down'
    else
      return 'none'
  }

  onSortClick(field : string){
    console.log(this.sort.replace('-',''));
    if(this.sort == field)
      this.sort = `-${field}`
    else if(this.sort.replace('-','') == field)
      this.sort = ''
    else
      this.sort = field

    this.getData()
    }
    
  ngOnChanges($event) {
    if ($event.columns && $event.columns.currentValue)
      this.settings = {
        ...this.settings,
        columns: $event.columns.currentValue,
      };
  }

  onCustomAction($event: any) {
    switch ($event.action) {
      case "view":
        this.onView.emit($event.data);
        console.log($event.data);
        break;

      case "edit":
        this.onEdit.emit($event.data);
        console.log($event.data);
        break;

      case "delete":
        console.log($event.data);
        this.onDelete.emit($event.data);
        break;

      default:
        break;
    }
  }

  getData() {
    // Initialize Params Object
    let params = new HttpParams();
    


    /**Create filters params */
    let filters = {}; 
    Object.keys(this.filtersForm.value).forEach((value)=>{
      if(!this.filtersForm.value[value])
        return
       filters[value] = this.filtersForm.value[value]
    })

    /**Create sort param */
    let sort = this.sort

    /**Create main query param */
    let query = {
      filter : filters,
      sort: sort ? sort : undefined
    }
    


    params = params.append("query", JSON.stringify(query));
    // params = params.append("secondParameter", this.filters.valueTwo);
    this.http.get(this.endpoint,{params: params}).subscribe((data) => {
      this.data.next(this.dataKey ? data[this.dataKey] : data);
      console.log(this.data.value)
    });
  }
}
