import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalDashboardTableComponent } from './local-dashboard-table.component';

describe('LocalDashboardTableComponent', () => {
  let component: LocalDashboardTableComponent;
  let fixture: ComponentFixture<LocalDashboardTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalDashboardTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalDashboardTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
