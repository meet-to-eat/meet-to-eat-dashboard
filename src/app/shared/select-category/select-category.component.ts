import { Ng2SmartTableComponent } from 'ng2-smart-table';
import { environment } from './../../../environments/environment.prod';
import { NbDialogRef } from '@nebular/theme';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ngx-select-category',
  templateUrl: './select-category.component.html',
  styleUrls: ['./select-category.component.scss']
})
export class SelectCategoryComponent{
  @ViewChild("dashboardTable", { static: false })
  dashboardTable: Ng2SmartTableComponent;

  endpoint: string;
  columns: {};

  constructor(
    private nbDialogRef : NbDialogRef<SelectCategoryComponent>
  ) {
    this.endpoint = `${environment.API_URL}/category`;
    this.columns = {
      image: {
        title: "Image",
        type: "html",
        valuePrepareFunction: (cell, row) => {
          return `<img class="avatar" src="${row.img_url}">`;
        },
      },
      name: {
        title: "Name",
        type: "string",
        valuePrepareFunction: (cell, row) => {
          return row.name;
        },
      },

      createdAt: {
        title: "Created At",
        type: "date",
        valuePrepareFunction: (cell, row) => {
          return row.created_at;
        },
      },

      updatedAt: {
        title: "Updated At",
        type: "date",
        valuePrepareFunction: (cell, row) => {
          return row.updated_at;
        },
      },
    };
  }


  onSubmit($event){
    console.log($event);
    this.nbDialogRef.close({name: $event.data.name,data: $event.data.id})
  }
}
