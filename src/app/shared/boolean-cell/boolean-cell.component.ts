import { Component, OnInit, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'ngx-boolean-cell',
  templateUrl: './boolean-cell.component.html',
  styleUrls: ['./boolean-cell.component.scss']
})
export class BooleanCellComponent implements OnInit, ViewCell {
 
  renderValue: boolean;

  @Input() value: string;
  @Input() rowData: any;

  ngOnInit() {
    this.renderValue = Boolean(this.value);
  }

  


}
