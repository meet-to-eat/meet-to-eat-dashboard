import { NbDialogService } from '@nebular/theme';
import { BehaviorSubject } from 'rxjs';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, ValidationErrors } from '@angular/forms';
import { ColorFormItem } from './form-items/color';

import { CountryISO, SearchCountryField, TooltipLabel } from 'ngx-intl-tel-input';
import { NumberFormItem } from './form-items/number-input';
import { BaseFormItem } from './form-items/base-item';
import { MultiSelectFormItem } from './form-items/multi-select';

function numbersValidator(control) {
  
  if(!control.value) return null;

  return isNumber(control.value) ? null : { 'error': 'not a number' }
}

function isNumber(value) {
  return /^[0-9]*(.[0-9]*)$/g.test(value)
}


@Component({
  selector: 'dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {
  _formItems;
  @Input() set formItems(formItems) {
    console.log('formItems changing')
    this.buildedFormItems = this.buildDynamicForm(formItems);
    this._formItems = formItems
  }
  @Output() onSubmit = new EventEmitter();
  @Input() disabled : boolean = false;
  @Input() hideSubmitButton: boolean;
  @Input() hideResetButton: boolean = false;

  buildedFormItems: FormGroup;

  CountryISO = CountryISO;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;

  constructor(private nbDialogService: NbDialogService) { }

  ngOnInit(): void {

  }


  public deleteItemsFromFormWithSpecificKeyIncludes(key : string): void {
    for(let control of Object.keys(this.buildedFormItems.controls))
      if(control.includes(key)){
        this.buildedFormItems.removeControl(control);
      }
    this._formItems = this._formItems.filter(x => !x.key.includes(key))
  }

  public addControlToFormGroup(controls): void {
    let tempFormGroup = this.buildDynamicForm(controls)
    for(let key of Object.keys(tempFormGroup.controls)){
      this.buildedFormItems.addControl(key,tempFormGroup.controls[key]);
    }
    this._formItems.push(...controls)
  }

  



  isEmptyObject(object) {
    return object && typeof object == 'object' && Object.keys(object).length == 0;
  }

  buildDynamicForm(formItems) {
    console.log('we are building the form now >>> ',formItems);
    let _formGroup = {};

    formItems.forEach(item => {


      switch (item._innerId) {
        case 'input':
          _formGroup[item.key] = new FormControl({value : item.value, disabled:item.disabled}, [...item.validators]);
          break;

        case 'textArea':
          _formGroup[item.key] = new FormControl({value : item.value, disabled:item.disabled}, [...item.validators]);
          break;

        case 'checkbox':
          _formGroup[item.key] = new FormControl({value : !!item.checked, disabled:item.disabled}, [...item.validators]);
          break;

        case 'dateRange':
          _formGroup[item.key] = new FormControl({value : {
            start: item.start,
            end: item.end
          }, disabled : item.disabled}, [...item.validators]);
          break;

        case 'multiSelect':
          _formGroup[item.key] = new FormControl({value : [...item.selectedList],disabled: item.disabled}, [...item.validators]);
          break;

        case 'color':
          _formGroup[item.key] = new FormControl({value : (item as ColorFormItem).color,disabled: item.disabled}, [...item.validators]);
          break;

        case 'number':
          _formGroup[item.key] = new FormControl({
            value: (item as NumberFormItem).value,
            disabled: item.disabled || false
          }, [
            numbersValidator,
            ...item.validators
          ], [...item.asyncValidators]);
          break;

        case 'dropdown':
          _formGroup[item.key] = new FormControl({value : item.selectedOption,disabled: item.disabled}, [...item.validators]);
          break;

        case 'dialog':
          _formGroup[item.key] = new FormControl({value :item.data,disabled: item.disabled}, [...item.validators]);
          break;

        case 'file':
          _formGroup[item.key] = new FormControl({value :item.data,disabled: item.disabled}, [...item.validators]);
        break;
  
        default:
            _formGroup[item.key] = new FormControl({value: item.value,disabled:item.disabled}, [...item.validators]);
          break;
      }

      // provide the form control object to the _interface
      item._interface = _formGroup[item.key];
    });

    return new FormGroup(_formGroup);
  }

  consol(data,name){
    console.log(name + '' , data);
    return data;
  }

  submitHandler() {
    
    Object.keys(this.buildedFormItems.controls).forEach(key => {

      console.log('before : ',this.buildedFormItems.valid)


      const controlErrors: ValidationErrors = this.buildedFormItems.get(key).errors;
      if (controlErrors != null) {
            Object.keys(controlErrors).forEach(keyError => {
              console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError],' keyCurrentValue : ',this.buildedFormItems.get(key));
            });
          }
        });

    for(let key of Object.keys(this.buildedFormItems.value)){
      if(this.buildedFormItems.value[key] == undefined || this.buildedFormItems.value[key] == [] || this.buildedFormItems.value[key] == '')
        delete this.buildedFormItems.value[key]
    }
    console.log('after : ',this.buildedFormItems,this.buildedFormItems.value)
    if (this.buildedFormItems.valid){
      console.log("submitting ... !!!",this.buildedFormItems.value);
      this.onSubmit.emit(this.buildedFormItems.value);
    }
  }

  resetForm($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();

    try {

      this._formItems
        .forEach(item => {

          if (item._innerId == 'multiSelect') {
            (item as MultiSelectFormItem).changeSelectedOptions([]);
          } else {
            this.buildedFormItems.controls[item.key].reset();
          }

        });

    } catch (error) {
      console.error(error);
    }
  }

  changeHandlerForNumberItem(value, formItem) {
    try {
      if (value == '') return;

      let parsedResult = parseFloat(String(value));

      if (isNaN(parsedResult)) {
        formItem._interface.setValue('');
        return;
      }

      if (!isNumber(value)) {
        formItem._interface.setValue(parseFloat(String(value)));
        return;
      }
    } catch (error) {
      console.error(error);
    }
  }

  public extractFormValues() {
    return this.buildedFormItems.getRawValue();
  }

  changeHandlerForDialogItem($event, formItem){
    let secondParameter : any = {hasScroll : true};
    if(formItem.context)
      secondParameter.context = formItem.context;
    let dialog = this.nbDialogService.open(formItem.component,secondParameter);
    dialog.onClose.subscribe((value) => {
      if(value && value.name)
      formItem.name = value.name;
      if(value && value.data){
        formItem.data = value.data;
        formItem.changeSelectedOption(value.data)
        
      }
    })
  }


  changeHandlerForFileItem($event, formItem){
    formItem.data = $event.target.files[0];
    formItem.changeFileUpload($event.target.files[0]);

  }

  console(value){
    console.log(value);
    return value;
  }
}

