import { BaseFormItem } from './base-item';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

/**
 * how to use
 * 
 * create new instance
 * 
 *     this.rate = new NumberFormItem({
 *                   key: 'rate',
 *                   value: '22',
 *                   placeholder: 'the rate',
 *                   showedLabel: 'The rate',
 *                   validators: [
 *                    Validators.required,
 *                    Validators.minLength(5),
 *                    Validators.maxLength(10),
 *                  ]
 *                 });
 * 
 * import Validator from angular
 *    import { Validators } from '@angular/forms';
 * 
 * Change the value via `changeValue` function
 *  
 *     this.rate.changeValue(55)
 *          
 * to get the current value use `getValue`
 * 
 *      this.rate.getValue()
 * 
 * 
 * listen for changes stream using `onChange` function which return a promise that return an observable 
 * of stream data which represent the changes
 * 
 *     this.rate.onChange()
 *     .then(valuesChangesObservable => {
 *       valuesChangesObservable
 *         .subscribe(newValue => console.warn('on name change', newValue))
 *     });
 * 
 */
export class NumberFormItem extends BaseFormItem {

    placeholder: string;
    disabled: boolean;

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'number',
        });

        if (initParams.placeholder)
            this.placeholder = initParams.placeholder;

        if (initParams.disabled)
            this.disabled = initParams.disabled;

    }


    changeValue(newValue) {
        (this._interface as FormControl).setValue(newValue);
    }

    onChange(): Promise<Observable<any>> {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && this._interface.valueChanges) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).valueChanges)
                }
            }, 1000);
        })
    }

    getValue() {
        return (this._interface as FormControl).value;
    }
}