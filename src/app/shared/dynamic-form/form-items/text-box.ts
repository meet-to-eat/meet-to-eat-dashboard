import { BaseFormItem } from './base-item';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';


/**
 * how to use
 * 
 * create new instance
 * 
 *     this.name = new TextBoxFormItem({
 *                   key: 'name',
 *                   value: 'hi',
 *                   type: 'text',
 *                   placeholder: 'the name',
 *                   showedLabel: 'The Name',
 * 
 *                   validators: [
 *                    Validators.required,
 *                    Validators.email,
 *                    Validators.minLength(3),
 *                    Validators.pattern(/^okay/g),
 *                    Validators.maxLength(10),
 *                    ({ value }) => {
 *                      return value.startsWith('hello') ? null : { 'error': 'must start with hello' }
 *                    }
 *                 ]
 *                 });
 *
 * import Validator from angular
 *    import { Validators } from '@angular/forms';
 * 
 * Change the text box value via `changeValue` function
 *  
 *     this.name.changeValue('new name')
 *          
 * to get the current value use `getValue`
 * 
 *      this.name.getValue()
 * 
 * 
 * listen for changes stream using `onChange` function which return a promise that return an observable 
 * of stream data which represent the changes
 * 
 *     this.name.onChange()
 *     .then(valuesChangesObservable => {
 *       valuesChangesObservable
 *         .subscribe(newValue => console.warn('on name change', newValue))
 *     });
 * 
 */
export class TextBoxFormItem extends BaseFormItem {
    type: string;
    placeholder: string;

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'input'
        });

        this.type = initParams.type;
        this.placeholder = initParams.placeholder;

    }

    changeValue(newValue) {
        (this._interface as FormControl).setValue(newValue);
    }

    onChange(): Promise<Observable<any>> {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && this._interface.valueChanges) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).valueChanges)
                }
            }, 1000);
        })
    }

    getValue() {
        return (this._interface as FormControl).value;
    }
}