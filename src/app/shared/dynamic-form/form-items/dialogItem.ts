import { BaseFormItem } from "./base-item";
import { FormControl } from "@angular/forms";
import { of, BehaviorSubject, Observable } from "rxjs";

export class DialogFormItem extends BaseFormItem {
  data: any;
  component: any;
  name: string;
  context: any;
  constructor(initParams) {
    super({
      ...initParams,
      value: initParams.value ? initParams.value.data : null,
      _innerId: "dialog",
    });

    if (initParams.component) this.component = initParams.component;
    if (initParams.context) this.context = initParams.context;

    if (initParams.value) {
      this.data = initParams.value.data;
      this.name = initParams.value.name;
    }
  }

  changeSelectedOption(newData) {
    this.data = newData.data;
    (this._interface as FormControl).setValue(newData);
  }

  onChange(): Promise<Observable<any>> {
    return new Promise((resolve, reject) => {
      let intervalToken = setInterval(() => {
        if (this._interface && this._interface.valueChanges) {
          clearInterval(intervalToken);
          resolve((this._interface as FormControl).valueChanges);
        }
      }, 1000);
    });
  }

  getSelectedOption() {
    return (this._interface as FormControl).value;
  }
}
