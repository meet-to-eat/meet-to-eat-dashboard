export class BaseFormItem{

    key: string;
    value: any;
    
    showedLabel: string;
    validators: any[];
    asyncValidators: any[];

    disabled : boolean;

    _innerId: string;
    _interface: any;

    constructor({ 
        key = '',
        value = '',

        showedLabel = '',
        validators = [],

        _innerId = '',
        asyncValidators = [],
        disabled = false,
    }){
        /**
         * this inner id will tell us how to render the item
         * if its 'input' then we know it's a noraml text input
         * if it's a 'dropdown' we know we need to render a dropdown and so on
         */
        this._innerId = _innerId;
        this.key = key;
        this.value = value;
        this.showedLabel = showedLabel;
        this.validators = validators;
        this.asyncValidators = asyncValidators;
        this.disabled = disabled;
    }
}