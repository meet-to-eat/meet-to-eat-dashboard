import { BaseFormItem } from './base-item';
import { BehaviorSubject } from 'rxjs';

import { debounceTime } from 'rxjs/operators';

/**
 * how to use
 * 
 * create new instance
 * 
 *     this.myColor = new ColorFormItem({
 *      key: 'myColor',
 *      color: '#dc2525',
 *      showedLabel: 'My Color',
 *      validators: [
 *         Validators.required
 *       ]
 *    });
 *
 * 
 * import Validator from angular
 *    import { Validators } from '@angular/forms';
 * 
 * listen for changes
 * 
 *    this.myColor.onChange()
 *    .subscribe(newColor => console.warn('new color ', newColor));
 *
 * 
 * get the current value
 *    this.myColor.getValue();
 *
 */
export class ColorFormItem extends BaseFormItem {

    color: string;
    colorSubject: BehaviorSubject<string> = new BehaviorSubject('#fff');

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'color'
        });

        if (initParams.color){
            this.color = initParams.color;
            this.colorSubject.next(initParams.color);
        }
        
    }

    _onColorChange($event){
        this._interface.setValue($event);
        this.colorSubject.next($event);
    }

    onChange(){
        return this.colorSubject
        .asObservable()
        .pipe(
            debounceTime(500)
        );
    }

    getValue(){
        return this.colorSubject.value;
    }
}