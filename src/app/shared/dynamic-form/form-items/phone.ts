import { BaseFormItem } from './base-item';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

/**
 * how to use
 * 
 * create new instance
 * 
 *     this.myPhone = new PhoneFormItem({
 *                     key: 'myPhone',
 *                     value: '+966 504 215 142',
 *                     showedLabel: 'The phone'
 * 
 *                     validators: [
 *                        Validators.required
 *                      ]
 *                   })
 *
 * import Validator from angular
 *    import { Validators } from '@angular/forms';
 *  
 * listen for changes
 * 
 *    this.myPhone.onChange()
 *    .then(changesObservable => {
 *      changesObservable.subscribe(newPhoneDate => {
 *        console.warn('new phone data: ', newPhoneDate);
 *      })
 *    });
 *
 * get current value
 * 
 *         this.myPhone.getValue()
 *         .then(phoneObjectValue => {
 *           console.warn('current value: ', phoneObjectValue)
 *         });
 * 
 */
export class PhoneFormItem extends BaseFormItem {

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'phone'
        });
    }


    onChange(): Promise<Observable<any>> {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && (this._interface as FormControl).valueChanges) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).valueChanges);
                }
            }, 1000);
        });
    }

    getValue() {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && (this._interface as FormControl).value) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).value);
                }
            }, 1000);
        });
    }

}