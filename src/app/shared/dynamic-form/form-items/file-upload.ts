import { BaseFormItem } from "./base-item";
import { FormControl } from "@angular/forms";
import { of, BehaviorSubject, Observable } from "rxjs";

export class FileFormItem extends BaseFormItem {
  data: any;
  file: any;
  name: string;
  constructor(initParams) {
    super({
      ...initParams,
      value: initParams.value,
      _innerId: "file",
    });

    if (initParams.value) {
        this.value = initParams.value; 
    }
  }

  changeFileUpload(newData) {
    this.data = newData;
    (this._interface as FormControl).setValue(newData);
  }

  onChange(): Promise<Observable<any>> {
    return new Promise((resolve, reject) => {
      let intervalToken = setInterval(() => {
        if (this._interface && this._interface.valueChanges) {
          clearInterval(intervalToken);
          resolve((this._interface as FormControl).valueChanges);
        }
      }, 1000);
    });
  }
}
