import { BaseFormItem } from './base-item';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';


/**
 * how to use
 * 
 * creating new instance
 * 
 *     this.isCool = new CheckboxItem({
 *                     key: 'iscool',
 *                     checked: true,
 *                     label: 'is it cool?',
 *                     validators: [
 *                      Validators.requiredTrue,
 *              
 *                      ({ value }) => {
 *                        if (value === false) return { 'error': 'must be true' }
 *                        else return null;
 *                      }
 *                    ]
 *                   });
 *
 * import Validator from angular
 *    import { Validators } from '@angular/forms';
 * 
 * change the current value
 * 
 *     this.isCool.changeValue(false);
 *
 * 
 * listen for value change
 * 
 *     this.isCool.onChange()
 *     .then(changesObservable => {
 *       changesObservable
 *       .subscribe(newValue => console.warn('listening...., new value is ', newValue));
 *     })
 * 
 */
export class CheckboxItem extends BaseFormItem {

    checked: boolean;
    label: string;

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'checkbox'
        });

        if (initParams.checked)
            this.checked = initParams.checked;

        if (initParams.label)
            this.label = initParams.label;
    }

    onChange(): Promise<Observable<any>> {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && this._interface.valueChanges) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).valueChanges);
                }
            }, 1000);
        });
    }

    changeValue(newValue) {
        let intervalToken = setInterval(() => {
            if (this._interface && this._interface.valueChanges) {
                clearInterval(intervalToken);
                (this._interface as FormControl).setValue(newValue);
            }
        }, 1000);
    }

    getValue() {
        return (this._interface as FormControl).value;
    }

}