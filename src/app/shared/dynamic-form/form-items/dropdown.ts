import { BaseFormItem } from './base-item';
import { FormControl } from '@angular/forms';
import { of, BehaviorSubject, Observable } from 'rxjs';

/**
 * how to use
 * 
 * create new instance 
 * 
 *        this.cities = new DropdownFormItem({
 *               key: 'cities',
 *               options: ['dubai', 'damascus', 'erbil'],
 *               selectedOption: '',
 *               showedLabel: 'Cities',
 *               validators: [
 *                 Validators.required
 *                ]
 *           });
 *
 * import Validator from angular
 *    import { Validators } from '@angular/forms';
 * 
 * you can the selected option via the function `changeSelectedOption`
 * and can be used as: 
 *              
 *          this.cities.changeSelectedOption('damascus')
 * 
 * 
 * you can change the options array using `changeOptions` function
 * 
 *          this.cities.changeOptions(['a', 'b', 'c']);
 *          
 * 
 * you can get the current selected option via `getSelectedOption` function
 * 
 *          this.cities.getSelectedOption()
 * 
 * 
 * you can subscribe to the event of new selected option using the `onChange` function
 * 
 *          this.cities.onChange()
 *          .subscribe(newSelectedOption => console.warn(newSelectedOption));
 * 
 */
export class DropdownFormItem extends BaseFormItem {

    options: any[];
    selectedOption: any;

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'dropdown'
        });

        if (initParams.options && initParams.options.length)
            this.options = initParams.options;

        if (initParams.selectedOption)
            this.selectedOption = initParams.selectedOption;

    }

    changeSelectedOption(newSelectedOption) {
        (this._interface as FormControl).setValue(newSelectedOption);
    }

    changeOptions(newOptions) {
        this.options = newOptions;
    }


    onChange(): Promise<Observable<any>> {
        
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && this._interface.valueChanges) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).valueChanges)
                }
            }, 1000);
        })
    }

    getSelectedOption() {
        return (this._interface as FormControl).value;
    }
} 