import { BaseFormItem } from './base-item';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

/**
 * how to use
 * 
 * create instance
 * 
 *       this.myBirthday = new DateFormItem({
 *         key: 'myBirthday',
 *         value: '',
 *         showedLabel: 'My Birthday',
 *         validators: [
 *    
 *            Validators.required,
 *    
 *            ({ value }) => {
 *    
 *              let myFatherBithday = new Date('December 17, 1955 03:24:00');
 *    
 *              // compare time stamps
 *              if (new Date(value).getTime() < myFatherBithday.getTime()) {
 *                return { 'error': 'must be bigger than my father birthday' }
 *              }
 *              return null;
 *            }
 *    
 *           ]
 *       });
 *
 * 
 * import Validator from angular
 *    import { Validators } from '@angular/forms';
 * 
 * get the current value
 *  
 *        this.myBirthday.getValue()
 *        .then(dateCurrentValue => console.warn(dateCurrentValue));
 *
 * 
 * set the date value
 * 
 *      this.myBirthday.setValue(new Date())
 * 
 * 
 * listen for changes
 * 
 *      this.myBirthday.onChange()
 *      .then(changesObservable => {
 *        changesObservable
 *        .subscribe(newDate => console.warn('listening for new date: ', newDate));
 *      })
 * 
 */
export class DateFormItem extends BaseFormItem {

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'date'
        })
    }

    changeValue(someDate) {
        let intervalToken = setInterval(() => {
            if (this._interface && (this._interface as FormControl).setValue) {
                clearInterval(intervalToken);
                (this._interface as FormControl).setValue(new Date(someDate));
            }
        }, 1000);
    }

    getValue(): Promise<any> {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && (this._interface as FormControl).valueChanges) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).value);
                }
            }, 1000);
        });
    }

    onChange(): Promise<Observable<any>> {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && (this._interface as FormControl).valueChanges) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).valueChanges);
                }
            }, 1000);
        });
    }
}