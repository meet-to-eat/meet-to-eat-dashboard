import { BaseFormItem } from './base-item';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormControl } from '@angular/forms';

/**
 * how to use
 * 
 * create new instance
 *     this.myFavourites = new MultiSelectFormItem({
 *                           key: 'myFavourites',
 *                           fullList: [
 *                               {key: 'A', value: 'A'},
 *                               {key: 'B', value: 'B'},
 *                           ],
 *                           selectedList: ['A'],
 *
 *                           showedLabel: 'My Favourites',
 *                           
 *                          validators: [
 *                              Validators.required,
 *                      
 *                              ({value}) => {
 *                                if(value.length < 2){
 *                                  return { 'error': 'must select more than 2 items' }
 *                                }
 *                                return null;
 *                              }
 *                            ]
 *                         })
 * 
 * import Validator from angular
 *    import { Validators } from '@angular/forms';
 * 
 * get currently selected items list
 * 
 *     this.myFavourites.getSelectedItems()
 *    .then(selectedList => {
 *      console.log('so far we have: ', selectedList);
 *    });
 *
 * 
 * onChange
 * 
 *    myFavourites.onChange()
 *   .then(observable => observable.subscribe(newSelectedList => console.log(newSelectedList)));
 * 
 * 
 * change selected options manually
 * 
 *    myFavourites.changeSelectedOptions(['World', 'Okay']);  
 * 
 */
export class MultiSelectFormItem extends BaseFormItem {

    fullList: [];
    selectedList: [];

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'multiSelect'
        });

        if (initParams.fullList)
            this.fullList = initParams.fullList;

        if (initParams.selectedList)
            this.selectedList = initParams.selectedList;
    }

    changeSelectedOptions(newOptions) {

        let intervalToken = setInterval(() => {
            if (this._interface) {
                clearInterval(intervalToken);
                (this._interface as FormControl).setValue(newOptions);
            }
        }, 1000);

    }

    onChange(): Promise<Observable<any>> {

        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && (this._interface as FormControl).value) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).valueChanges);
                }
            }, 1000);
        });
    }

    getSelectedItems() {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && (this._interface as FormControl).value) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).value);
                }
            }, 1000);
        });
    }

}