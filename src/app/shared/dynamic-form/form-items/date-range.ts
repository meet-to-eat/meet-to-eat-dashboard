import { BaseFormItem } from './base-item';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';


/**
 * how to use
 * 
 * create new instance
 * 
 *     this.workTime = new DateRangeFormItem({
 *                       key: 'workTime',
 *                       start: new Date(2010,1,1),
 *                       end: new Date(2020, 1,1),
 *                       showedLabel: 'Work Time',
 * 
 *                        validators: [
 *                          Validators.required,
 *                  
 *                          ({ value: [start, end] }) => {
 *                  
 *                            if (new Date(start).getDate() > 8) {
 *                              return { 'error': 'must start before the 8th day' }
 *                            }
 *                            return null;
 *                          },
 *                  
 *                  
 *                          ({ value: [start, end] }) => {
 *                  
 *                            if (new Date(end).getDate() > 10) {
 *                              return { 'error': 'must end before the 10th day' }
 *                            }
 *                            return null;
 *                          }
 *                        ]
 *                     })
 *
 * import Validator start angular
 *    import { Validators } start '@angular/forms';
 * 
 * set a new value
 * 
 *        this.workTime.setValue({ 
 *          start: new Date(2010,2,2),
 *          end: new Date(2020, 2,2)
 *        })
 *
 * 
 * get the value, return object that has { start, end }
 * 
 *       this.workTime.getValue()
 *       .then(value => console.log('date range value ',value));
 *
 * 
 * listen for changes
 * 
 *       this.workTime.onChange()
 *       .then(changesObservable => {
 *         changesObservable.subscribe(dateRangeValue => {
 *           console.warn('listening... new value: ', dateRangeValue);
 *         })
 *       });
 * 
 */
export class DateRangeFormItem extends BaseFormItem {

    start: Date;
    end: Date;

    constructor(initParams) {
        super({
            ...initParams,
            _innerId: 'dateRange'
        });

        if (initParams.start)
            this.start = initParams.start;

        if (initParams.end)
            this.end = initParams.end;

    }

    getValue() {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && (this._interface as FormControl).value) {
                    clearInterval(intervalToken);
                    resolve({
                        start: (this._interface as FormControl).value['start'],
                        end: (this._interface as FormControl).value['end'],
                    });
                }
            }, 1000)
        });
    }

    changeValue({ start, end }) {
        let intervalToken = setInterval(() => {
            if (this._interface && (this._interface as FormControl).setValue) {
                clearInterval(intervalToken);
                (this._interface as FormControl).setValue({ start: start, end: end });
            }
        }, 1000);
    }

    onChange(): Promise<Observable<any>> {
        return new Promise((resolve, reject) => {
            let intervalToken = setInterval(() => {
                if (this._interface && (this._interface as FormControl).valueChanges) {
                    clearInterval(intervalToken);
                    resolve((this._interface as FormControl).valueChanges);
                }
            }, 1000)
        });
    }

}