import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardTableComponent } from './dashboard-table/dashboard-table.component';

import { NbCardModule, NbIconModule, NbInputModule, NbButtonModule, NbCheckboxModule, NbDatepickerModule, NbSelectModule, NbAlertModule, NbDialogModule, NbTableModule, NbTabsetModule, NbStepperModule, NbRadioModule, NbToggleModule, NbSpinnerModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { ColorCircleComponent } from './color-circle/color-circle.component';
import { BooleanCellComponent } from './boolean-cell/boolean-cell.component';
import { DateCellComponent } from './date-cell/date-cell.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { TranslatePipe } from '../translation/translate.pipe';
import { TranslateModule } from '../translation/translate/translate.module';
import { ColorPickerModule } from 'ngx-color-picker';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { DynamicFormErrorsComponent } from './dynamic-form-errors/dynamic-form-errors.component';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { CheckboxFilterComponent } from './checkbox-filter/checkbox-filter.component';
import { DateFilterComponent } from './date-filter/date-filter.component'
import { TempCustomFilterComponent } from './temp-custom-filter/temp-custom-filter.component';
import { CustomListFilterComponent } from './custom-list-filter/custom-list-filter.component';
import { CustomDateFilterComponent } from './custom-date-filter/custom-date-filter.component';
import { PurchaseOrderStatusCellComponent } from './purchase-order-status-cell/purchase-order-status-cell.component';
import { CustomDualFilterComponent } from './custom-dual-filter/custom-dual-filter.component';
import { StatusCellComponent } from './status-cell/status-cell.component';
import { LocalDashboardTableComponent } from './local-dashboard-table/local-dashboard-table.component';
import { DataViewComponent } from './data-view/data-view.component';
import { DataViewRecursivelyComponent } from './data-view-recursively/data-view-recursively.component'
import { DialogWith3TextInputComponent } from './dialog-with3-text-input/dialog-with3-text-input.component';
import { SelectCategoryComponent } from './select-category/select-category.component';
import { SelectPermissionsComponent } from './select-permissions/select-permissions.component';


@NgModule({
  declarations: [
    DashboardTableComponent,
    ColorCircleComponent,
    BooleanCellComponent,
    DateCellComponent,
    DynamicFormComponent,
    DynamicFormComponent,
    DynamicFormErrorsComponent,
    DeleteModalComponent,
    CheckboxFilterComponent,
    DateFilterComponent,
    TempCustomFilterComponent,
    CustomListFilterComponent,
    CustomDateFilterComponent,
    PurchaseOrderStatusCellComponent,
    CustomDualFilterComponent,
    StatusCellComponent,
    LocalDashboardTableComponent,
    DataViewComponent,
    DataViewRecursivelyComponent,
    DialogWith3TextInputComponent,
    SelectCategoryComponent,
    SelectPermissionsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    // NbCardModule,
    NbToggleModule,
    // NbButtonModule,

    TranslateModule,

    NbCardModule,
    NbIconModule,
    NbInputModule,
    Ng2SmartTableModule,

    NbEvaIconsModule,
    NbIconModule,
    NbButtonModule,

    NbInputModule,
    NbCheckboxModule,

    NbDatepickerModule,
    NbSelectModule,
    NbAlertModule,
    NbDialogModule,
    ColorPickerModule,


    BsDropdownModule.forRoot(),
    NgxIntlTelInputModule,

    NbTabsetModule,

    NbStepperModule,
    NbSpinnerModule
  ],
  providers: [
  ],
  entryComponents: [
    TempCustomFilterComponent,
    CustomListFilterComponent,
    CustomDateFilterComponent,
    CustomDualFilterComponent,
  ],
  exports: [
    DashboardTableComponent,
    DynamicFormComponent,

    FormsModule,
    ReactiveFormsModule,

    NbCardModule,
    NbToggleModule,
    NbIconModule,
    NbInputModule,
    Ng2SmartTableModule,
    NbDialogModule,

    NbEvaIconsModule,
    NbIconModule,
    NbButtonModule,

    TranslatePipe,
    NbTabsetModule,

    NbStepperModule,

    NbCheckboxModule,
    NbDatepickerModule,
    NbSelectModule,
    NbAlertModule,
    NbRadioModule,

    DeleteModalComponent,
    CustomListFilterComponent,
    CustomDateFilterComponent,
    CustomDualFilterComponent,

    LocalDashboardTableComponent,
    DataViewComponent,
    DataViewRecursivelyComponent,
    NbSpinnerModule,

  ]
})
export class SharedModule { }
