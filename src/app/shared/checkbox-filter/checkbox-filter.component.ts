import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { DefaultFilter } from 'ng2-smart-table';
import { pipe } from 'rxjs';

@Component({
  selector: 'ngx-checkbox-filter',
  templateUrl: './checkbox-filter.component.html',
  styleUrls: ['./checkbox-filter.component.scss']
})
export class CheckboxFilterComponent extends DefaultFilter implements OnInit, OnChanges {

  formControl: FormControl;

  constructor() {
    super();
  }


  ngOnInit(): void {
    this.formControl = new FormControl(false);
    this.formControl.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(this.delay))
      .subscribe(
        (value) => {
          
          if (value == true && this.column.filter.config)
            this.query = this.column.filter.config.checked || value.toString();
          else if (value == false && this.column.filter.config)
            this.query = this.column.filter.config.unchecked || value.toString();
          else this.query = '';

          this.setFilter();
        }
      );
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  reset() {
    this.formControl.setValue(null);
  }

}
