import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NbDialogRef } from '@nebular/theme';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';

@Component({
  selector: 'ngx-dialog-with3-text-input',
  templateUrl: './dialog-with3-text-input.component.html',
  styleUrls: ['./dialog-with3-text-input.component.scss']
})
export class DialogWith3TextInputComponent implements OnInit {
  
    @Input() title;
    @Input() message;
    @Input() onAccept;
    @Input() inputsMetaData:any[];
    isinputMetaDataExist:boolean = false;
    result : any[];
    myGroup:any;
    formItems: BehaviorSubject<any[]> = new BehaviorSubject([]);
    constructor(
      protected ref: NbDialogRef<DeleteModalComponent>
    ) { }
  
    ngOnInit(): void {
      console.log(this.inputsMetaData);
      if(this.inputsMetaData.length != 0 && this.inputsMetaData != null)
      {
        this.isinputMetaDataExist = true;
      }
  
    }
  
    dismiss(){
      this.ref.close(this.result != undefined ? this.result : null);
    }
  
    onAcceptHandler(){
      this.result = this.inputsMetaData.map( (values:any) => ({ [values['key']]: values['value']})   );
      this.onAccept();
      this.dismiss();
    }
  }
  

