import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogWith3TextInputComponent } from './dialog-with3-text-input.component';

describe('DialogWith3TextInputComponent', () => {
  let component: DialogWith3TextInputComponent;
  let fixture: ComponentFixture<DialogWith3TextInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogWith3TextInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWith3TextInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
