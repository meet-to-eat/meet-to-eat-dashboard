import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataViewRecursivelyComponent } from './data-view-recursively.component';

describe('DataViewRecursivelyComponent', () => {
  let component: DataViewRecursivelyComponent;
  let fixture: ComponentFixture<DataViewRecursivelyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataViewRecursivelyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataViewRecursivelyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
