import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Renderer2,
} from "@angular/core";
import { isObject, isArray } from "util";

@Component({
  selector: "data-view-recursively",
  templateUrl: "./data-view-recursively.component.html",
  styleUrls: ["./data-view-recursively.component.scss"],
})
export class DataViewRecursivelyComponent implements OnInit {
  @ViewChild("tree") tree: ElementRef;

  @Input() set object(value) {
    this._object = this.fillObject(value,0);
    this.fillTree(this.tree.nativeElement, 0, this._object);
  }

  @Input() ignore : any[];
  _object;

  constructor(private renderer: Renderer2) {}

  fillObject(object,level) {
    let result = [];
    for (let key of Object.keys(object)) {
      if(this.ignore.includes(key) && level == 0)
        continue;
      if (!isObject(object[key])) result.push({ key, value: object[key] });
      else result.push({ key, value: this.fillObject(object[key],level + 1) });
    }
    console.log(object)
    return result;
  }

  fillTree(
    parentElement: HTMLElement,
    level: number,
    objects: { key: string; value: any }[]
  ) {
    for (let object of objects) {
      let fieldElement = this.renderer.createElement("div");
      let titleElement = this.renderer.createElement("div");
      let titleText = this.renderer.createText(object.key);
      if(level != 0){
        let titleWithHorizontalLine = this.renderer.createElement('div');
        this.renderer.addClass(titleWithHorizontalLine,'title-with-horizontal-line-container');

        let horizontalLine = this.renderer.createElement('div')
        this.renderer.addClass(horizontalLine,'horizontal-line')
        this.renderer.appendChild(titleWithHorizontalLine,horizontalLine)
        this.renderer.appendChild(titleWithHorizontalLine,titleElement)
        this.renderer.appendChild(fieldElement,titleWithHorizontalLine)

      }
      else{
        this.renderer.appendChild(fieldElement, titleElement);
        this.renderer.addClass(fieldElement,'with-border-field')
      }
      this.renderer.appendChild(titleElement,titleText)
      this.renderer.addClass(titleElement, "title");
      if (!isArray(object.value)) {
        if(level == 0){
          this.renderer.addClass(fieldElement, "field");

        }
        else{
          this.renderer.addClass(fieldElement, "in-children-field");
          this.renderer.setStyle(fieldElement,'width',`calc(100% - ${level * 53}px)`)
        }

        let valueElement = this.renderer.createElement("div");
        let valueText = this.renderer.createText(object.value);

        this.renderer.appendChild(valueElement,valueText)
        this.renderer.addClass(valueElement, "value");
        this.renderer.appendChild(fieldElement, valueElement);
      
      } else {

        this.renderer.addClass(fieldElement, "children-field");
        if(level != 0){
          this.renderer.addClass(fieldElement, "in-children-field-and-has-a-children");
        }
        let childrenElement = this.renderer.createElement('div');
        this.renderer.addClass(childrenElement,'children');
        this.renderer.appendChild(fieldElement,childrenElement)
        this.fillTree(childrenElement,level+1,object.value);

      }
      this.renderer.appendChild(parentElement,fieldElement)
    }
  }

  ngOnInit(): void {}
}
