import { Component, OnInit, Input } from '@angular/core';
import { fadeInTrigger } from '../animation';

@Component({
  selector: 'dynamic-form-errors',
  templateUrl: './dynamic-form-errors.component.html',
  styleUrls: ['./dynamic-form-errors.component.scss'],
  animations: [ fadeInTrigger ]
})
export class DynamicFormErrorsComponent implements OnInit {

  @Input() formItem;


  constructor() { }

  ngOnInit(): void { }

  keys(obj) {
    return Object.keys(obj);
  }

}
