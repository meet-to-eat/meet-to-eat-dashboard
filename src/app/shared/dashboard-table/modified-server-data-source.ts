import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import { map } from "rxjs/operators";
import { LocalDataSource } from "ng2-smart-table";
import { getDeepFromObject } from "@nebular/auth";

export class ModifiedServerDataSource extends LocalDataSource {
  protected conf;

  protected lastRequestCount: number = 0;

  constructor(protected http: HttpClient, conf) {
    super();

    this.conf = conf;

    if (!this.conf.endPoint) {
      throw new Error(
        "At least endPoint must be specified as a configuration of the server data source."
      );
    }
  }

  count(): number {
    return this.lastRequestCount;
  }

  getElements(): Promise<any> {
    return this.requestElements()
      .pipe(
        map((res) => {
          this.lastRequestCount = this.extractTotalFromResponse(res);
          this.data = this.extractDataFromResponse(res);

          return this.data;
        })
      )
      .toPromise();
  }

  /**
   * Extracts array of data from server response
   * @param res
   * @returns {any}
   */
  protected extractDataFromResponse(res: any): Array<any> {
    const rawData = res.body;
    const data = !!this.conf.dataKey
      ? getDeepFromObject(rawData, this.conf.dataKey, [])
      : rawData;

    if (data instanceof Array) {
      return data;
    }

    throw new Error(`Data must be an array.
    Please check that data extracted from the server response by the key '${this.conf.dataKey}' exists and is array.`);
  }

  /**
   * Extracts total rows count from the server response
   * Looks for the count in the heders first, then in the response body
   * @param res
   * @returns {any}
   */
  protected extractTotalFromResponse(res: any): number {
    if (res.headers.has(this.conf.totalKey)) {
      return +res.headers.get(this.conf.totalKey);
    } else {
      const rawData = res.body;
      return getDeepFromObject(rawData, this.conf.totalKey, 0);
    }
  }

  protected requestElements(): Observable<any> {
    let httpParams = this.createRequesParams();
    return this.http.get(this.conf.endPoint, {
      params: httpParams,
      observe: "response",
    });
  }

  protected createRequesParams(): HttpParams {
    let httpParams = new HttpParams();

    httpParams = this.addSortRequestParams(httpParams);
    httpParams = this.addFilterRequestParams(httpParams);
    console.log(httpParams.keys());
    console.log(httpParams.get('filter'));
    return this.addPagerRequestParams(httpParams);
  }

  protected addSortRequestParams(httpParams: HttpParams): HttpParams {
    if (this.sortConf) {
      this.sortConf.forEach((fieldConf) => {
        if (fieldConf.direction.toUpperCase() == "ASC")
          httpParams = httpParams.set(this.conf.sortKey, fieldConf.field);
        else if (fieldConf.direction.toUpperCase() == "DESC")
          httpParams = httpParams.set(this.conf.sortKey, '-' + fieldConf.field);
      });
    }

    return httpParams;
  }

  protected addFilterRequestParams(httpParams: HttpParams): HttpParams {
    let filters = {};
    if(this.conf.preFilters){
      for(let filter of this.conf.preFilters)
      {
        console.log('filters',filter)
        httpParams.set(filter.key,filter.value)
      }// filters[filter.key] = filter.value;
    }
    // if (this.filterConf.filters && this.filterConf.filters.length) {
    //   this.filterConf.filters.forEach((fieldConf: any) => {
    //     console.log(fieldConf["search"])
    //     if (fieldConf["search"]) {
    //       filters[fieldConf["field"]] = fieldConf["search"]
    //     }
    //   });
    // }
    // if(this.conf.preFilters || (this.filterConf.filters && this.filterConf.filters.length))
    //   httpParams = httpParams.set(this.conf.filterKey,JSON.stringify(filters));
    return httpParams;
  }

  protected addPagerRequestParams(httpParams: HttpParams): HttpParams {
    if (
      this.pagingConf &&
      this.pagingConf["page"] &&
      this.pagingConf["perPage"]
    ) {
      httpParams = httpParams.set(
        this.conf.pagerPageKey,
        this.pagingConf["page"]
      );
      httpParams = httpParams.set(
        this.conf.pagerLimitKey,
        this.pagingConf["perPage"]
      );
    }


    return httpParams;
  }
}
