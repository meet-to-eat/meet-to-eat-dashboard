import { RoleProviderService } from "./../../services/role-provider.service";
import { ModifiedServerDataSource } from "./modified-server-data-source";
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import _ from "lodash";

export enum DashboardTableActions {
  CREATE,
  DELETE,
  VIEW_ONE,
  UPDATE,
}

@Component({
  selector: "dashboard-table",
  templateUrl: "./dashboard-table.component.html",
  styleUrls: ["./dashboard-table.component.scss"],
})
export class DashboardTableComponent implements OnInit, OnChanges {
  @Input() endpoint: string;
  @Input() actions: { permissions: string[]; action: DashboardTableActions }[] = [];
  @Input() columns: {};
  @Input() enableSelection: boolean = false;
  @Input() enableCheckList: boolean = false;
  @Input() set checkListData(value: any[]) {
    this.addCheckListData(value);
  }
  @Input() preFilters : any[];
  @Input() pageLimit : number = 10;

  @Output() onAdd = new EventEmitter();
  @Output() onEdit = new EventEmitter();
  @Output() onDelete = new EventEmitter();
  @Output() onView = new EventEmitter();
  @Output() onSubmitCheckList = new EventEmitter();
  @Output() onSubmitSelection = new EventEmitter();
@Input() dataKey = 'data';
@Input() anotherActions = [];
  @Output() onCustomActionClick = new EventEmitter();

  selectedRows: any = {};

  settings = {
    actions: {
      custom: [],
      add: false,
      edit: false,
      delete: false,
    },
    mode: "external",
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    editButtonContent: '<i class="nb-edit"></i>',
      edit: {
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {},
    pager: {
      display: true,
      perPage: this.pageLimit,
    },
    // hideSubHeader: true,
  };

  source: ModifiedServerDataSource;
  source2: ClientData;

  constructor(
    private http: HttpClient,
    private roleProviderService: RoleProviderService
  ) {}

  addCheckListData(value) {
    if (this.enableCheckList)
      for (let s of value) {
        this.selectedRows[s] = true;
      }
  }
  ngOnInit(): void {
    console.log(this.checkListData);
    this.settings.actions = {
      ...this.settings.actions,
      ...this.actions,
    };
    this.settings.columns = {
      ...this.columns,
    };
    this.source = new ModifiedServerDataSource(this.http, {
      endPoint: String(this.endpoint),
      dataKey: this.dataKey,
      pagerPageKey: "page",
      sortFieldKey: "orderBy",
      sortDirKey: "sortDir",
      pagerLimitKey: "per_page",
      sortKey: "sort",
      filterKey: "filter",
      totalKey: "total",
      preFilters: this.preFilters,
    });
    this.settings["rowClassFunction"] = (row) => {
      let cssClass = "";
      if (this.selectedRows[row.data.id] && this.enableCheckList) {
        cssClass += "selected-row";
      }
      if (row.data.active != undefined && !row.data.active)
        cssClass += " disabled-row";
      return cssClass;
    };
    let isThereAnAction = false;

    if (
      this.actions
        .map((x) => x.action)
        .includes(DashboardTableActions.VIEW_ONE) &&
      this.roleProviderService.isGranted(
        this.actions.filter(
          (x) => x.action == DashboardTableActions.VIEW_ONE
        )[0].permissions
      )
    ) {
      this.settings.actions["custom"].push({
        name: "view",
        title: '<i class="eva eva-eye-outline"></i>',
      });
      isThereAnAction = true;
    }

    if (
      this.actions
        .map((x) => x.action)
        .includes(DashboardTableActions.UPDATE) &&
      this.roleProviderService.isGranted(
        this.actions.filter((x) => x.action == DashboardTableActions.UPDATE)[0]
          .permissions
      )
    ) {
      this.settings.actions["custom"].push({
        name: "edit",
        title: '<i class="eva eva-edit-outline"></i>',
      });
      isThereAnAction = true;
    }

    if (
      this.actions
        .map((x) => x.action)
        .includes(DashboardTableActions.DELETE) &&
      this.roleProviderService.isGranted(
        this.actions.filter((x) => x.action == DashboardTableActions.DELETE)[0]
          .permissions
      )
    ) {
      this.settings.actions["custom"].push({
        name: "delete",
        title: '<i class="eva eva-power-outline"></i>',
      });
      isThereAnAction = true;
    }

    if (
      this.actions.map((x) => x.action).includes(DashboardTableActions.CREATE) &&
      this.roleProviderService.isGranted(
        this.actions.filter((x) => x.action == DashboardTableActions.CREATE)[0]
          .permissions
      )
    ) {
      this.settings.actions.add = true;
      isThereAnAction = true;
    }

    if(this.anotherActions && this.anotherActions.length > 0){
      for(let action of this.anotherActions)
      this.settings.actions["custom"].push(action);
      isThereAnAction = true;

    }
    if (!isThereAnAction) (this.settings as any)["actions"] = false;
  }
  ngOnChanges($event) {
    if ($event.columns && $event.columns.currentValue)
      this.settings = {
        ...this.settings,
        columns: $event.columns.currentValue,
      };
  }

  onCustomAction($event: any) {
    switch ($event.action) {
      case "view":
        this.onView.emit($event.data);
        break;

      case "edit":
        this.onEdit.emit($event.data);
        break;

      case "delete":
        this.onDelete.emit($event.data);
        break;
      default:
        break;
    }

    for(let action of this.anotherActions)
      if($event.action == action.name){
        this.onCustomActionClick.emit({name : action.name,data: $event.data});
        console.log("blablabla");
      }
  }

  onSelectRow($event) {
    if (this.enableCheckList) {
      if (!this.selectedRows[$event.data.id]) {
        this.selectedRows[$event.data.id] = true;
      } else delete this.selectedRows[$event.data.id];
    } else if (this.enableSelection) {
      this.submitSelection($event);
    }
  }

  onSubmit() {
    this.onSubmitCheckList.emit(Object.keys(this.selectedRows));
  }

  submitSelection(value) {
    this.onSubmitSelection.emit(value);
  }
}
