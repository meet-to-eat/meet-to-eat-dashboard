import { Component, OnInit, Input } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

@Component({
  selector: "data-view",
  templateUrl: "./data-view.component.html",
  styleUrls: ["./data-view.component.scss"],
})
export class DataViewComponent implements OnInit {
  @Input() object;
  @Input() ignore;
  @Input() children;
  @Input() rules;

  constructor(private sr: DomSanitizer) {}

  ngOnInit(): void {}

  getItems() {
    return this.extractPropertyFromObject(
      this.object,
      this.ignore,
      this.children,
      this.rules
    );
  }

  extractPropertyFromObject(
    object,
    ignore?: string[],
    children?: { field: string; key: string | string[] }[],
    rules?: { key: string; html: string }[]
  ): { key: string; value: string }[] {
    let properties: { key: string; value: string }[] = [];
    if (!object) return properties;

    let keys = Object.keys(object);

    for (let key of keys) {
      /**check if it in ignore list */
      if (
        (ignore && ignore.includes(key)) ||
        object[key] == undefined ||
        object[key] == "" ||
        key == undefined
      ) {
        // console.log(object[key]);
        continue;
      }

      let newKey = this.getKeyPerfectName(key);
      let newValue;
      /**check if the key is in children list */
      if (children && children.map((x) => x.field).includes(key)) {
        newValue = this.extractChildrenFromObject(object, key, children);
      } else newValue = object[key];
      if (rules && rules.map((x) => x.key).includes(key)) {
        newValue = this.getByRule(newValue, key, rules);
      }
      properties.push({ key: newKey, value: newValue });
    }

    return properties;
  }

  getByRule(curr, key, rules) {
    return rules.filter((x) => x.key == key)[0].html.replace("#field#", curr);
  }

  extractChildrenFromObject(object, key, children) {
    let value;
    let fieldKeys: any = children.filter((x) => x.field == key)[0].key;

    value = "";
    for (let k of fieldKeys) {
      let keys = k.split(".");
      let tempObject = object;
      for (let kk of keys) {
        tempObject = tempObject[kk];
      }
      value = value + object[key][k] + " ";
    }
    return value;
  }

  getKeyPerfectName(tName) {
    let name = tName;
    name = name.replace("_", "");
    name = name.slice(0, 1).toUpperCase() + name.slice(1, name.length);
    // name[0] = name[0].toUpperCase();
    let newName = name;
    for (let i = 0; i < name.length; i++)
      if ("Z" >= name[i] && "A" <= name[i] && i != 0)
        newName = name.slice(0, i) + " " + name.slice(i, name.length + 2);
    return newName;
  }

  getSafeHTML(value): SafeHtml {
    return this.sr.bypassSecurityTrustHtml(value);
  }
}
