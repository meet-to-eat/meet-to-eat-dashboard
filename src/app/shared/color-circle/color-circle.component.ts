import { Component, OnInit, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'ngx-color-circle',
  templateUrl: './color-circle.component.html',
  styleUrls: ['./color-circle.component.scss']
})
export class ColorCircleComponent implements OnInit, ViewCell {
 
  renderValue: string;

  @Input() value: string;
  @Input() rowData: any;

  ngOnInit() {
    this.renderValue = this.value;
  }

}
