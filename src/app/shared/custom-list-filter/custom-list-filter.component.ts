import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { DefaultFilter } from 'ng2-smart-table';
import { FormControl } from '@angular/forms';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'ngx-custom-list-filter',
  templateUrl: './custom-list-filter.component.html',
  styleUrls: ['./custom-list-filter.component.scss']
})
export class CustomListFilterComponent extends DefaultFilter implements OnInit {
  listControl = new FormControl();
  
  options: any = []

  constructor() {
    super();
  }

  ngOnInit() {
    this.listControl.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(200),
      )
      .subscribe((value: any) => {
        console.warn(value);
        // this.query = value !== null ? this.inputControl.value.toString() : '';
        this.query = value !== null ? '222' : '';
        this.setFilter();
      });
  }

  setOptions(options) {
    this.options = options;
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (changes.query) {
      // this.query = changes.query.currentValue;
      this.query = '333';
      // this.inputControl.setValue(this.query);
    }
  }
}