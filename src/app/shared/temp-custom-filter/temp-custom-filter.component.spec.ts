import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempCustomFilterComponent } from './temp-custom-filter.component';

describe('TempCustomFilterComponent', () => {
  let component: TempCustomFilterComponent;
  let fixture: ComponentFixture<TempCustomFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempCustomFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempCustomFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
