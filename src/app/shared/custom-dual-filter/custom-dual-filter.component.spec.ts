import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomDualFilterComponent } from './custom-dual-filter.component';

describe('CustomDualFilterComponent', () => {
  let component: CustomDualFilterComponent;
  let fixture: ComponentFixture<CustomDualFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomDualFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomDualFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
