import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { DefaultFilter } from 'ng2-smart-table';
import { FormControl } from '@angular/forms';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'ngx-custom-dual-filter',
  templateUrl: './custom-dual-filter.component.html',
  styleUrls: ['./custom-dual-filter.component.scss']
})
export class CustomDualFilterComponent extends DefaultFilter implements OnInit, OnChanges {

  moreThan: FormControl;
  lessThan: FormControl;

  constructor() { 
    super(); 
  }

  ngOnInit(): void {
    this.moreThan = new FormControl(null);
    this.lessThan = new FormControl(null);

    this.moreThan.valueChanges.pipe(filter((value) => value != null))
      .subscribe(
        (value) => {
          let id = this.column.id;
          if (this.column.filter.config) this.column.id = this.column.filter.config.form_1 || 'moreThan';
          else this.column.id = 'moreThan';
          this.query = new Number(value).toString();
          this.setFilter();
          this.column.id = id;
        },
        (error) => console.error(error)
      );

    this.lessThan.valueChanges.pipe(filter((value) => value != null))
      .subscribe(
        (value) => {
          let id = this.column.id;
          if (this.column.filter.config) this.column.id = this.column.filter.config.form_2 || 'lessThan';
          else this.column.id = 'lessThan';
          this.query = new Number(value).toString();
          this.setFilter();
          this.column.id = id;
        },
        (error) => console.error(error)
      );
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  resetFilters() {
    let id = this.column.id;

    this.moreThan.setValue(null);
    this.lessThan.setValue(null);
    
    this.column.id = this.column.filter.config.form_1;
    this.query = null;
    this.setFilter();

    this.column.id = this.column.filter.config.form_2;
    this.query = null;
    this.setFilter();

    this.column.id = id;

  }

}
