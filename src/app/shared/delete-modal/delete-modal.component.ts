import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { BehaviorSubject } from 'rxjs';
import { TextBoxFormItem } from '../dynamic-form/form-items/text-box';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'ngx-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {

  @Input() title;
  @Input() message;
  @Input() onDelete;

  myGroup:any;
  formItems: BehaviorSubject<any[]> = new BehaviorSubject([]);
  constructor(
    protected ref: NbDialogRef<DeleteModalComponent>
  ) { }

  ngOnInit(): void {
  }

  dismiss(){
    this.ref.close();
  }

  onDeleteHandler(){
    this.onDelete();
    this.dismiss();
  }
}
