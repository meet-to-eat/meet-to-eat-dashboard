import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { MainPageComponent } from './admin-pages/main-page/main-page.component';
import { NotFoundPageComponent } from './admin-pages/not-found-page/not-found-page.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: MainPageComponent,
    },
    {
      path: 'roles',
      loadChildren: () => import('./admin-pages/roles/roles.module').then(module => module.RolesModule)
    },
    {
      path: 'categories',
      loadChildren: () => import('./admin-pages/categories/categories.module').then(module => module.CategoriesModule)
    },
    {
      path: 'permissions',
      loadChildren: () => import('./admin-pages/permissions/permissions.module').then(module => module.PermissionsModule)
    },
    {
      path: 'ingredients',
      loadChildren: () => import('./admin-pages/ingredients/ingredients.module').then(module => module.ingredientsModule)
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundPageComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
