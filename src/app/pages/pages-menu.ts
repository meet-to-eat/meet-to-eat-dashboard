import { NbMenuItem } from '@nebular/theme';
import { LUCKY_CARD_READ, ROLE_READ, CURRENCY_READ, SERVICE_PROVIDER_READ, PERMISSION_READ, PERMISSION_UPDATE, LUCKY_CARD_CREATE, LUCKY_CARD_READ_ONE, LUCKY_CARD_UPDATE, LUCKY_CARD_DELETE, CURRENCY_CREATE, CURRENCY_DELETE, CURRENCY_READ_ONE, CURRENCY_UPDATE, ROLE_CREATE, ROLE_UPDATE, ROLE_DELETE, MERCHANT_PAYMENT_ACCEPT, PACKAGE_READ, PACKAGE_TYPE_READ, USER_READ, MERCHANT_READ, MERCHANT_PAYMENT_READ } from '../utils/permissions.constants';
import { AnySoaRecord, AnyRecordWithTtl, AnyPtrRecord } from 'dns';

/**
 * you can change the icon from nebular-icons
 *
 * https://github.com/akveo/nebular-icons/tree/master/src/icons
 */
export const MENU_ITEMS: any[] = [

 
  {
    title: "Meet To Eat",
    group: true,
  },
  {
    title: 'Categories',
    icon: 'layout-outline',
    link: '/pages/categories',
  },
  {
    title: "Roles",
    icon: "layout-outline",
    link: "/pages/roles",
    // permissions: [ROLE_READ]
  },
  {
    title: "Ingredients",
    icon: "layout-outline",
    link: "/pages/ingredients",
    // permissions: [ROLE_READ]
  },
  {
    title: "Permissions",
    icon: "layout-outline",
    link: "/pages/permissions",
    permissions: [PERMISSION_READ]
  },
  // {
  //   title: "Auth",
  //   icon: "lock-outline",
  //   children: [
  //     {
  //       title: "Login",
  //       link: "/auth/login",
  //     },
  //     {
  //       title: "Register",
  //       link: "/auth/register",
  //     },
  //     {
  //       title: "Request Password",
  //       link: "/auth/request-password",
  //     },
  //     {
  //       title: "Reset Password",
  //       link: "/auth/reset-password",
  //     },
  //   ],
  // },
];
