import { RoleProviderService } from './../services/role-provider.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PagesMenuService {

  constructor(private roleProvider: RoleProviderService){}
  public getPages(menuItems){
    menuItems = JSON.parse(JSON.stringify(menuItems));
    let pages = []
    for(let item of menuItems){
      // console.log(item.title,item.permissions,(!item.permissions) || this.roleProvider.isGranted(item.permissions,item.operator || 'and'))
      if((!item.permissions) || this.roleProvider.isGranted(item.permissions,item.operator || 'and')){
        if(item.children){
          item.children = this.getPages(item.children)

        }
        // delete item.permissions; delete item.operator;
        pages.push(item);
      }
    }

    // console.log('pages : ' ,pages);
    return pages;
  }
}
