import { PagesMenuService } from './pages-menu.service';
import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './pages-menu'
import { NbLayoutDirectionService } from '@nebular/theme';
import { TranslateService } from '../translation/translate.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu | async"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {

  menu: BehaviorSubject<any>;

  language: Observable<string>;

  constructor(
    private directionService: NbLayoutDirectionService,
    private translateService: TranslateService,
    private pagesMenuService : PagesMenuService 
    // private jwtHelperService: JwtHelperService
  ) {
    this.menu = new BehaviorSubject(pagesMenuService.getPages(MENU_ITEMS))
    this.language =
      this.directionService.onDirectionChange()
        .pipe(
          map(direction => direction == 'ltr' ? 'en' : 'ar')
        );

    this.language
      .subscribe(language => this.updateMenu(language));
      
  }

  ngOnInit(){

      // console.log(this.jwtHelperService.decodeToken(JSON.parse(localStorage.getItem('auth_app_token')).value))
  }


  updateMenu(language) {
    let newMenu = this.nestedTranslator(this.pagesMenuService.getPages(MENU_ITEMS), language)
    this.menu.next(newMenu);
  }


  nestedTranslator(menu, language) {

    return menu.map(item => {

      if (item.children && item.children.length)
        return {
          ...item,
          title: this.translateService.translate(item.title, language),
          children: this.nestedTranslator(item.children, language)
        }
      else
        return {
          ...item,
          title: this.translateService.translate(item.title, language),
        }
    });
  }


}
