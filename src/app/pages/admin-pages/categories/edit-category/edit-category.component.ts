import { DialogFormItem } from './../../../../shared/dynamic-form/form-items/dialogItem';
import { FileFormItem } from './../../../../shared/dynamic-form/form-items/file-upload';
import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { CategoriesService } from '../categories.service';
import { ActivatedRoute, Router, Params, Route } from '@angular/router';
import { CheckboxItem } from '../../../../shared/dynamic-form/form-items/checkbox';
import { Validators } from '@angular/forms';
import { TextBoxFormItem } from '../../../../shared/dynamic-form/form-items/text-box';
import { NumberFormItem } from '../../../../shared/dynamic-form/form-items/number-input';
import { ColorFormItem } from '../../../../shared/dynamic-form/form-items/color';
import { SelectCategoryComponent } from '../../../../shared/select-category/select-category.component';

@Component({
  selector: 'ngx-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {
  
  category;
  formItems;

  constructor(
    private categoriesService: CategoriesService,
    private activatedRoute :ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(async (params)=>{
      let categoryId = params['categoryId']
      if(categoryId){
        this.category = (await this.categoriesService.getCategoryById(categoryId) as any).category
        console.log(this.category)
        this.buildAllForms();
      }
    })
  }

  buildAllForms(){
    console.log(this.category);
    let ar_name = new TextBoxFormItem({
      key: "ar_name",
      type: "text",
      value: this.category.ar_name,
      placeholder: "name in arabic",
      showedLabel: "Ar Name",
      validators: [Validators.required],
    });

    let en_name = new TextBoxFormItem({
      key: "en_name",
      type: "text",
      value: this.category.en_name,
      placeholder: "name in english",
      showedLabel: "En Name",
      validators: [Validators.required],
    });

    let parent_id = new DialogFormItem({
      key: "parent_id",
      type: "text",
      component: SelectCategoryComponent,
      // value: {name: this.category.parent.name,data: this.category.parent.id},
      placeholder: "Parent",
      showedLabel: "Parent",
      validators: [Validators.required],
    });

    let img = new FileFormItem({
      key: "img",
      type: "file",
      value: "",
      showedLabel: "Image",
      validators: [Validators.required],
    });

    this.formItems = [
      ar_name,
      en_name,
      img,
      parent_id,
    ];
  }
  

  onSubmit($event){
    console.log($event);
    if($event.img){
      let formData:FormData = new FormData();
          formData.append('img', $event.img, $event.img.name);
          formData.append('ar_name', $event.ar_name);
          formData.append('en_name', $event.en_name);
          formData.append('parent_id', $event.parent_id);
  
      this.categoriesService.addCategory(formData).then(()=>{
        this.router.navigateByUrl('/pages/categories')
      });
    }
    else
    this.categoriesService.addCategory($event);

  }

}
