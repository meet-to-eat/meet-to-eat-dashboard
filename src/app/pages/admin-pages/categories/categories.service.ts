import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpWrapperService } from '../../../services/http-wrapper.service';


@Injectable({
  providedIn: 'root'
})

export class CategoriesService {

  constructor(
    private httpWrapper: HttpWrapperService
  ) { }

  getPagedCategories(params = {}) {
    return this.httpWrapper.get({
      url: `${environment.API_URL}/Categories/paged`,
      params
    });
  }

  getAllCategories(params = {}) {
    return this.httpWrapper.get({
      url: `${environment.API_URL}/Categories`,
      params
    });
  }

  getCategoryById(categoryId) {
    return this.httpWrapper.get({
      url: `${environment.API_URL}/category/${categoryId}/edit`
    });
  }

  addCategory(category) {
    return this.httpWrapper.post({
      url: `${environment.API_URL}/category`,
      body: category
    });
  }

  editCategory(category) {
    return this.httpWrapper.put({
      url: `${environment.API_URL}/category/${category.id}`,
      body: category
    });
  }

  deleteCategoryById(id) {
    if(!id) return Promise.reject('no id provided');

    return this.httpWrapper.delete({
      url: `${environment.API_URL}/category/${id}`
    });
  }

  getCategoriesColumns() {

    return [
      { prop: 'id', name: 'ID', width: 50 },
      { prop: 'keyName', name: 'Key Name', },
      { prop: 'isActive', name: 'Is Active', width: 100 },
      { prop: 'hasSub', name: 'Has Sub', type: 'boolean' },
      { prop: 'color', name: 'Color', width: 100 },
      { prop: 'imageUrl', name: 'Image' },
      { prop: 'sequenceNumberAndroid', name: 'Sequence Number Android', width: 200 },
      { prop: 'sequenceNumberIos', name: 'Sequence Number Ios' },
      { prop: 'parentId', name: 'Parent ID', width: 75 },
      { prop: 'parentName', name: 'Parent Name' },
      { prop: 'adjustTokenIos', name: 'Adjust Token Ios' },
      { prop: 'adjustTokenAndroid', name: 'Adjust Token Android' },
      { prop: 'showAtWholeSaler', name: 'Show At Whole Saler', type: 'boolean' },
      { prop: 'showAtSite', name: 'Show At Site', type: 'boolean' },
      { prop: 'showAtAndroid', name: 'Show At Android', type: 'boolean' },
      { prop: 'showAtIos', name: 'Show At Ios', type: 'boolean' },
      { prop: 'showAtKisok', name: 'Show At Kisok', type: 'boolean' },
      { prop: 'updatedAt', name: 'Updated At' },
      { prop: 'updatedBy', name: 'Updated By' },

      { prop: 'nameEn', name: 'English Name' },
      { prop: 'nameAr', name: 'Arabic Name' },

      { prop: 'actions', name: 'Actions', width: 250, frozenLeft:true  }
    ];

  }


}
