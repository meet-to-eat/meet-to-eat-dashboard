import { DashboardTableActions } from "./../../../../shared/dashboard-table/dashboard-table.component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../../../environments/environment";
import { ColorCircleComponent } from "../../../../shared/color-circle/color-circle.component";
import { BooleanCellComponent } from "../../../../shared/boolean-cell/boolean-cell.component";
import { DateCellComponent } from "../../../../shared/date-cell/date-cell.component";
import { NbDialogService } from "@nebular/theme";
import { DeleteModalComponent } from "../../../../shared/delete-modal/delete-modal.component";
import { CategoriesService } from "../categories.service";
import { Ng2SmartTableComponent } from "ng2-smart-table";
import { CustomDateFilterComponent } from "../../../../shared/custom-date-filter/custom-date-filter.component";

@Component({
  selector: "ngx-view-categories",
  templateUrl: "./view-categories.component.html",
  styleUrls: ["./view-categories.component.scss"],
})
export class ViewCategoriesComponent implements OnInit {
  @ViewChild("dashboardTable", { static: false })
  dashboardTable: Ng2SmartTableComponent;

  endpoint: string;
  actions =  [
    { action: DashboardTableActions.CREATE, permissions: [] },
    { action: DashboardTableActions.UPDATE, permissions: [] },
    { action: DashboardTableActions.DELETE, permissions: [] },
  ];
  columns: {};

  constructor(
    private router: Router,
    private nbDialogService: NbDialogService,
    private categoriesService: CategoriesService
  ) {
    this.endpoint = `${environment.API_URL}/category`;
    this.columns = {
      image: {
        title: "Image",
        type: "html",
        valuePrepareFunction: (cell, row) => {
          return `<img class="avatar" src="${row.img_url}">`;
        },
      },
      name: {
        title: "Name",
        type: "string",
        valuePrepareFunction: (cell, row) => {
          return row.name;
        },
      },

      createdAt: {
        title: "Created At",
        type: "date",
        valuePrepareFunction: (cell, row) => {
          return row.created_at;
        },
      },

      updatedAt: {
        title: "Updated At",
        type: "date",
        valuePrepareFunction: (cell, row) => {
          return row.updated_at;
        },
      },
    };
  }

  ngOnInit(): void {}

  onAdd(data) {
    this.router.navigateByUrl("/pages/categories/add");
  }

  onEdit(data) {
    this.router.navigateByUrl(`/pages/categories/edit/${data.id}`);
  }

  onDelete(category) {
    this.nbDialogService.open(DeleteModalComponent, {
      context: {
        title: "Are you sure you want to delete this category?",
        message: `Category: ${category.name}`,
        onDelete: () => {
          this.categoriesService
            .deleteCategoryById(category.id)
            .then((response) => {
              console.log(response);
              this.dashboardTable.source.refresh();
            })
            .catch((error) => console.error(error));
        },
      },
    });
  }

  onView(data) {
    this.router.navigateByUrl(`/pages/categories/view/${data.id}`);
  }
}
