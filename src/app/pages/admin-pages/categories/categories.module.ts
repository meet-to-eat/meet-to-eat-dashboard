import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewCategoriesComponent } from './view-categories/view-categories.component';
import { ViewCategoryComponent } from './view-category/view-category.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { SharedModule } from '../../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ViewCategoriesComponent
  },
  {
    path: 'edit/:categoryId',
    component: EditCategoryComponent
  },
  {
    path: 'view/:categoryId',
    component: ViewCategoryComponent
  },
  {
    path: 'add',
    component: AddCategoryComponent
  }
];

@NgModule({
  declarations: [ViewCategoriesComponent, ViewCategoryComponent, AddCategoryComponent, EditCategoryComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class CategoriesModule { }
