import { DialogFormItem } from './../../../../shared/dynamic-form/form-items/dialogItem';
import { FileFormItem } from './../../../../shared/dynamic-form/form-items/file-upload';
import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriesService } from '../categories.service';
import { TextBoxFormItem } from '../../../../shared/dynamic-form/form-items/text-box';
import { Validators } from '@angular/forms';
import { SelectCategoryComponent } from '../../../../shared/select-category/select-category.component';

@Component({
  selector: 'ngx-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  formItems;

  constructor(
    private categoriesService: CategoriesService,
    private route: Router,
  ) { }

  ngOnInit(): void {
    this.buildAllForms();
  }

  buildAllForms(){
    let ar_name = new TextBoxFormItem({
      key: "ar_name",
      type: "text",
      value: "",
      placeholder: "name in arabic",
      showedLabel: "Ar Name",
      validators: [Validators.required],
    });

    let en_name = new TextBoxFormItem({
      key: "en_name",
      type: "text",
      value: "",
      placeholder: "name in english",
      showedLabel: "En Name",
      validators: [Validators.required],
    });

    let parent_id = new DialogFormItem({
      key: "parent_id",
      type: "text",
      component: SelectCategoryComponent,
      placeholder: "Parent",
      showedLabel: "Parent",
    });

    let img = new FileFormItem({
      key: "img",
      type: "file",
      value: "",
      showedLabel: "Image",
      validators: [Validators.required],
    });

    this.formItems = [
      ar_name,
      en_name,
      img,
      parent_id,
    ];
  }
  

  onSubmit($event){
    console.log($event);
    let formData:FormData = new FormData();
        formData.append('img', $event.img, $event.img.name);
        formData.append('ar_name', $event.ar_name);
        formData.append('en_name', $event.en_name);
        formData.append('parent_id', $event.parent_id);

    this.categoriesService.addCategory(formData).then(()=>{
      this.route.navigateByUrl('pages/categories')
    });
    
  }

}
