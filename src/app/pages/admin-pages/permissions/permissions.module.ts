import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { ViewPermissionsComponent } from './view-permissions/view-permissions.component';
import { EditPermissionComponent } from './edit-permission/edit-permission.component';
import { PermissionsService } from './permissions.service';
import { GeneralPermissionsGuard } from '../../../services/general-permissions-guard.service';
import { PERMISSION_READ, PERMISSION_UPDATE } from '../../../utils/permissions.constants';

const routes: Routes = [
  {
    path: '',
    component: ViewPermissionsComponent,
    canActivate: [GeneralPermissionsGuard],
    data: {
      permissions: [PERMISSION_READ]
    }
  },
  {
    path: 'edit/:permissionId',
    component: EditPermissionComponent,
    canActivate: [GeneralPermissionsGuard],
    data: {
      permissions: [PERMISSION_UPDATE]
    }
  }
];

@NgModule({
  declarations: [ViewPermissionsComponent,EditPermissionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  providers:[
    PermissionsService
  ]
})
export class PermissionsModule { }
