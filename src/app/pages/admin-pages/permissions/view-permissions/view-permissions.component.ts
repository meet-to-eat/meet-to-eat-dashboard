import { Component, OnInit, ViewChild } from '@angular/core';
import { BooleanCellComponent } from '../../../../shared/boolean-cell/boolean-cell.component';
import { DateCellComponent } from '../../../../shared/date-cell/date-cell.component';
import { environment } from '../../../../../environments/environment';
import { Router } from '@angular/router';
import { PermissionsService } from '../permissions.service';
import { DashboardTableComponent, DashboardTableActions } from '../../../../shared/dashboard-table/dashboard-table.component';
import { Ng2SmartTableComponent } from 'ng2-smart-table';
import { PERMISSION_UPDATE } from './../../../../utils/permissions.constants';
@Component({
  selector: 'ngx-view-permissions',
  templateUrl: './view-permissions.component.html',
  styleUrls: ['./view-permissions.component.scss']
})
export class ViewPermissionsComponent implements OnInit {
 
  endpoint: string;
  actions: any[];
  columns: any = undefined;

  @ViewChild('dashboardTable', { static: false }) dashboardTable: Ng2SmartTableComponent;

  constructor(private router: Router,private permissionService:PermissionsService) { 
    this.endpoint = `${environment.API_URL}/permissions`;
    this.actions = [{ permissions: [PERMISSION_UPDATE], action: DashboardTableActions.UPDATE },];
    this.initColumns();

  }

  ngOnInit(): void {
  }

  initColumns() {
    this.columns = {
      name: { title: 'Permission', type: 'string'},
      active: { title: 'active',  type: 'custom', renderComponent: BooleanCellComponent },
      createdAt: { title: 'Created At',   type: 'custom', renderComponent: DateCellComponent, filter: false, sort: false },
      updatedAt: { title: 'Updated At',   type: 'custom', renderComponent: DateCellComponent, filter: false, sort: false },
    }
  }

  onEdit(data){
    console.warn('onEdit', data);
    this.router.navigateByUrl(`/pages/permissions/edit/${data['_id']}`);
  }
}
