import { Injectable } from '@angular/core';
import { HttpWrapperService } from '../../../services/http-wrapper.service';
import { environment } from '../../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  constructor(private http: HttpWrapperService) { }

  getPermissionById(permissionId:string ,lang = 'en'){
    return this.http.get({
      url: `${environment.API_URL}/permissions/${permissionId}`,
      headers: new HttpHeaders({ 'Accept-Language': lang })
    });
  }

  getPermissions(params){
    return this.http.get({url: `${environment.API_URL}/permissions`,params});
  }

  addPermission(PermissionData) {

    console.log(PermissionData);
    return this.http.post({
        url: `${environment.API_URL}/permissions`,
        body: PermissionData,
        headers: {},
        params: {}
    });
}

UpdatePermission(permissionId:string,PermissionData:any){
  console.log(permissionId);
  console.log(PermissionData);

  return this.http.put({
    url: `${environment.API_URL}/permissions/${permissionId}`,
    body: PermissionData,
    headers: {},
    params: {}
});
}

UpdatePermissionActiviation(permissionId:number,active:boolean){
  return this.http.put({
    url: `${environment.API_URL}/permissions/${permissionId}/active`,
    body: { active }
})
}

}
