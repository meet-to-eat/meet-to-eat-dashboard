import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PermissionsService } from './../permissions.service'
import { TextBoxFormItem } from '../../../../shared/dynamic-form/form-items/text-box';
import { Validators } from '@angular/forms';
import { TextAreaFormItem } from '../../../../shared/dynamic-form/form-items/text-area';
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: 'ngx-edit-permission',
  templateUrl: './edit-permission.component.html',
  styleUrls: ['./edit-permission.component.scss']
})
export class EditPermissionComponent implements OnInit {

  permissionId:string;
  formItems: BehaviorSubject<any[]> = new BehaviorSubject([]);
  
  constructor(private activateRoute:ActivatedRoute,
    private permissionService:PermissionsService
    ) { }

  ngOnInit(): void {
    this.permissionId = this.activateRoute.snapshot.paramMap.get("permissionId");
    this.permissionService.getPermissionById(this.permissionId).then((val) => {
      let permissionData = val['data'];
      
      this.buildForm(permissionData['name'],permissionData['description']);
      console.log(permissionData);
    });
   
  }

  onSubmit($event) {
    const permissionData = Object.assign(
      $event
    );

    this.permissionService.UpdatePermission(this.permissionId,permissionData)

  }


  buildForm(permissionName:string,permissionDescription:string){
    
    const name = new TextBoxFormItem({
      key: 'name',
      type: 'text',
      value: permissionName,
      placeholder: 'Package Name',
      showedLabel: 'Package Name',
      validators: [Validators.required]
    });
    
    const description = new TextAreaFormItem({
      key: 'description',
      type: 'text',
      value: permissionDescription,
      placeholder: 'Description',
      showedLabel: 'Description',
      validators: []
    });

    this.formItems.next([
      name,
      description
    ]);
  }
}
