import { DashboardTableActions } from "./../../../../shared/dashboard-table/dashboard-table.component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../../../environments/environment";
import { ColorCircleComponent } from "../../../../shared/color-circle/color-circle.component";
import { BooleanCellComponent } from "../../../../shared/boolean-cell/boolean-cell.component";
import { DateCellComponent } from "../../../../shared/date-cell/date-cell.component";
import { NbDialogService } from "@nebular/theme";
import { DeleteModalComponent } from "../../../../shared/delete-modal/delete-modal.component";
import { IngredientsService } from "../ingredients.service";
import { Ng2SmartTableComponent } from "ng2-smart-table";
import { CustomDateFilterComponent } from "../../../../shared/custom-date-filter/custom-date-filter.component";

@Component({
  selector: "ngx-view-ingredients",
  templateUrl: "./view-ingredients.component.html",
  styleUrls: ["./view-ingredients.component.scss"],
})
export class ViewIngredientsComponent implements OnInit {
  @ViewChild("dashboardTable", { static: false })
  dashboardTable: Ng2SmartTableComponent;
  filter: boolean = false
  preFilters = [{ key: "not_approved", value: true }];
  endpoint: string;
  actions =  [
    { action: DashboardTableActions.CREATE, permissions: [] },
    { action: DashboardTableActions.UPDATE, permissions: [] },
    { action: DashboardTableActions.DELETE, permissions: [] },
  ];
  columns: {};
  anotherActions = [{
    name: "approve",
    title: '<i class="eva eva-checkmark-circle-outline"></i>',
  }]
  constructor(
    private router: Router,
    private nbDialogService: NbDialogService,
    private ingredientsService: IngredientsService
  ) {
    this.endpoint = `${environment.API_URL}/ingredient`;
    this.columns = {
      name: {
        title: "Name",
        type: "string",
        valuePrepareFunction: (cell, row) => {
          return row.name;
        },
      },

      createdBy: {
        title: "Added By",
        type: "string",
        valuePrepareFunction: (cell, row) => {
          return row.added_by ? row.added_by.first_name + ' ' + row.added_by.last_name : '';
        },
      },

      updatedBy: {
        title: "Updated By",
        type: "string",
        valuePrepareFunction: (cell, row) => {
          return row.edited_by ? row.edited_by.first_name + ' ' + row.edited_by.last_name : '';
        },
      },
      ApprovedAt: {
        title: "Approved At",
        type: "string",
        valuePrepareFunction: (cell, row) => {
          return row.approved_by ? row.approved_by.first_name + ' ' + row.approved_by.last_name : '';
        },
      },
      createdAt: {
        title: "Created At",
        type: "date",
        valuePrepareFunction: (cell, row) => {
          return row.created_at;
        },
      },

      updatedAt: {
        title: "Updated At",
        type: "date",
        valuePrepareFunction: (cell, row) => {
          return row.updated_at;
        },
      },
  
    };
  }

  ngOnInit(): void {}

  onAdd(data) {
    this.router.navigateByUrl("/pages/ingredients/add");
  }

  onEdit(data) {
    this.router.navigateByUrl(`/pages/ingredients/edit/${data.id}`);
  }

  onChange($event){
    this.dashboardTable.source.refresh();
  }

  onDelete(category) {
    this.nbDialogService.open(DeleteModalComponent, {
      context: {
        title: "Are you sure you want to delete this category?",
        message: `Category: ${category.name}`,
        onDelete: () => {
          this.ingredientsService
            .deleteIngredientById(category.id)
            .then((response) => {
              console.log(response);
              this.dashboardTable.source.refresh();
            })
            .catch((error) => console.error(error));
        },
      },
    });
  }

  onCustomAction($event){
    console.log($event);
    this.ingredientsService.approve($event.data.id).then(()=>{
      this.dashboardTable.source.refresh();

    });
  }

  onView(data) {
    this.router.navigateByUrl(`/pages/categories/view/${data.id}`);
  }
}
