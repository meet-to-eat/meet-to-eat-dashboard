import { Component, OnInit } from '@angular/core';
import { IngredientsService } from '../ingredients.service';
import { ActivatedRoute, Router, Params, Route } from '@angular/router';
import { Validators } from '@angular/forms';
import { TextBoxFormItem } from '../../../../shared/dynamic-form/form-items/text-box';

@Component({
  selector: 'ngx-edit-ingredient',
  templateUrl: './edit-ingredient.component.html',
  styleUrls: ['./edit-ingredient.component.scss']
})
export class EditIngredientComponent implements OnInit {
  
  ingredient;
  formItems;

  constructor(
    private ingredientsService: IngredientsService,
    private activatedRoute :ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(async (params)=>{
      let IngredientId = params['ingredientId']
      if(IngredientId){
        console.log(IngredientId)
        this.ingredient = (await this.ingredientsService.getIngredientById(IngredientId) as any).ingredient;
        console.log(this.ingredient);
        this.buildAllForms();
      }
    })
  }

  buildAllForms(){
    let ar_name = new TextBoxFormItem({
      key: "ar_name",
      type: "text",
      value: this.ingredient.ar_name,
      placeholder: "name in arabic",
      showedLabel: "Ar Name",
      validators: [Validators.required],
    });

    let en_name = new TextBoxFormItem({
      key: "en_name",
      type: "text",
      value: this.ingredient.en_name,
      placeholder: "name in english",
      showedLabel: "En Name",
      validators: [Validators.required],
    });

    this.formItems = [
      ar_name,
      en_name,
    ];
  }
  

  onSubmit($event){
    this.ingredientsService.addIngredient($event).then(()=>{
      this.router.navigateByUrl('pages/ingredients')
    });;

  }

}
