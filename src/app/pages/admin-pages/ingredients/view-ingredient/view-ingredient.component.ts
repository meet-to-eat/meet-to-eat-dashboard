import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-view-ingredient',
  templateUrl: './view-ingredient.component.html',
  styleUrls: ['./view-ingredient.component.scss']
})
export class ViewIngredientComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
