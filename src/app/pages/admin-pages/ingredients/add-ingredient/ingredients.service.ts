import { environment } from '../../../../../environments/environment.prod';
import { HttpWrapperService } from '../../../../services/http-wrapper.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private httpWrapper: HttpWrapperService){}
  
  addCategory(category) {
    return this.httpWrapper.post({
      url: `${environment.API_URL}/categories`,
      body: category
    });
  }


  editServiceProvider(serviceProvider) {
    return this.httpWrapper.put({
      url: `${environment.API_URL}/categories/${serviceProvider.serviceProviderId}`,
      body: serviceProvider
    });
  }

  getServiceProviderById(id) {
    if(!id) return Promise.reject('no id provided');

    return this.httpWrapper.get({
      url: `${environment.API_URL}/categories/${id}`
    });
  }


  changeActiveStateOfServiceProviderById(id,active) {
    if(!id) return Promise.reject('no id provided');

    return this.httpWrapper.put({
      url: `${environment.API_URL}/categories/${id}/active`
    ,body : {active}});
  }}
