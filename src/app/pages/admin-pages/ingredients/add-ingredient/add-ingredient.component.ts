import { DialogFormItem } from './../../../../shared/dynamic-form/form-items/dialogItem';
import { FileFormItem } from './../../../../shared/dynamic-form/form-items/file-upload';
import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { IngredientsService } from '../ingredients.service';
import { CheckboxItem } from '../../../../shared/dynamic-form/form-items/checkbox';
import { TextBoxFormItem } from '../../../../shared/dynamic-form/form-items/text-box';
import { Validators } from '@angular/forms';
import { NumberFormItem } from '../../../../shared/dynamic-form/form-items/number-input';
import { ColorFormItem } from '../../../../shared/dynamic-form/form-items/color';
import { SelectCategoryComponent } from '../../../../shared/select-category/select-category.component';

@Component({
  selector: 'ngx-add-ingredient',
  templateUrl: './add-ingredient.component.html',
  styleUrls: ['./add-ingredient.component.scss']
})
export class AddIngredientComponent implements OnInit {

  formItems;

  constructor(
    private ingredientsService: IngredientsService,
    private route: Router,
  ) { }

  ngOnInit(): void {
    this.buildAllForms();
  }

  buildAllForms(){
    let ar_name = new TextBoxFormItem({
      key: "ar_name",
      type: "text",
      value: "",
      placeholder: "name in arabic",
      showedLabel: "Ar Name",
      validators: [Validators.required],
    });

    let en_name = new TextBoxFormItem({
      key: "en_name",
      type: "text",
      value: "",
      placeholder: "name in english",
      showedLabel: "En Name",
      validators: [Validators.required],
    });

    this.formItems = [
      ar_name,
      en_name,
    ];
  }
  

  onSubmit($event){
    this.ingredientsService.addIngredient($event).then(()=>{
      this.route.navigateByUrl('pages/ingredients')
    });
    
  }

}
