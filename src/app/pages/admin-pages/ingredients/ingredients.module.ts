import { EditIngredientComponent } from './edit-ingredient/edit-ingredient.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { AddIngredientComponent } from './add-ingredient/add-ingredient.component';
import { ViewIngredientComponent } from './view-ingredient/view-ingredient.component';
import { ViewIngredientsComponent } from './view-ingredients/view-ingredients.component';

const routes: Routes = [
  {
    path: '',
    component: ViewIngredientsComponent
  },
  {
    path: 'edit/:ingredientId',
    component: EditIngredientComponent
  },
  {
    path: 'view/:ingredientId',
    component: ViewIngredientComponent
  },
  {
    path: 'add',
    component: AddIngredientComponent
  }
];

@NgModule({
  declarations: [ViewIngredientsComponent, EditIngredientComponent, ViewIngredientComponent, AddIngredientComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class ingredientsModule { }
