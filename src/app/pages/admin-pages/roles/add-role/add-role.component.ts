import { DialogFormItem } from './../../../../shared/dynamic-form/form-items/dialogItem';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TextBoxFormItem } from '../../../../shared/dynamic-form/form-items/text-box';
import { Validators } from '@angular/forms';
import {RolesService} from './../roles.service';
import { Router } from '@angular/router';
import { NbGlobalPhysicalPosition, NbToastRef, NbToastrService } from '@nebular/theme';
import { SelectPermissionsComponent } from '../../../../shared/select-permissions/select-permissions.component';

@Component({
  selector: 'ngx-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})
export class AddRoleComponent implements OnInit {

  formItems;

  constructor(
    private rolesService: RolesService,
    private route: Router,
  ) { }

  ngOnInit(): void {
    this.buildAllForms();
  }

  buildAllForms(){
    let name = new TextBoxFormItem({
      key: "name",
      type: "text",
      value: "",
      placeholder: "name",
      showedLabel: "Name",
      validators: [Validators.required],
    });

    let description_ar = new TextBoxFormItem({
      key: "description_ar",
      type: "text",
      value: "",
      placeholder: "description ar",
      showedLabel: "Description Ar",
      validators: [Validators.required],
    });

    let description_en = new TextBoxFormItem({
      key: "description_en",
      type: "text",
      value: "",
      placeholder: "description en",
      showedLabel: "Description En",
      validators: [Validators.required],
    });

    let permissions = new DialogFormItem({
      key: "permissions_ids",
      component: SelectPermissionsComponent,
      placeholder: "Permissions",
      showedLabel: "Permissions",
      validators: [Validators.required],
    });

    this.formItems = [
      name, description_ar, description_en,permissions
    ];
  }
  

  onSubmit($event){
    this.rolesService.addNewRole($event).then(()=>{
      this.route.navigateByUrl('pages/roles')
    });
    
  }

}
