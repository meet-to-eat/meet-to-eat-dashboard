import { DashboardTableActions, DashboardTableComponent } from './../../../../shared/dashboard-table/dashboard-table.component';
import { ROLE_UPDATE, ROLE_READ_ONE, ROLE_DELETE, ROLE_CREATE } from './../../../../utils/permissions.constants';
import { Component, OnInit, ViewChild } from '@angular/core';
import { RolesService } from '../roles.service';
import { environment } from '../../../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { DateCellComponent } from '../../../../shared/date-cell/date-cell.component';
import { Router } from '@angular/router';
import { BooleanCellComponent } from '../../../../shared/boolean-cell/boolean-cell.component';
import { DeleteModalComponent } from '../../../../shared/delete-modal/delete-modal.component';
import { NbDialogService, NbGlobalPhysicalPosition, NbToastRef, NbToastrService } from '@nebular/theme';
@Component({
  selector: 'ngx-view-roles',
  templateUrl: './view-roles.component.html',
  styleUrls: ['./view-roles.component.scss']
})

export class ViewRolesComponent implements OnInit {

  endpoint: string;
  actions: any[];
  columns: any = undefined;
  roles: BehaviorSubject<any[]> = new BehaviorSubject([]);
  toastRef: NbToastRef;
  @ViewChild('dashboardTable', { static: false }) dashboardTable: DashboardTableComponent;
  
  constructor(
    private roleService:RolesService,
    private router: Router,
    private nbDialogService: NbDialogService,
    private toastrService: NbToastrService
    ) { 
      this.endpoint = `${environment.API_URL}/role`;
      this.actions = [
        { permissions: [], action: DashboardTableActions.UPDATE },
        { permissions: [], action: DashboardTableActions.VIEW_ONE },
        { permissions: [], action: DashboardTableActions.DELETE },
        { permissions: [], action: DashboardTableActions.CREATE },
      ];     
       this.initColumns();
  }

  ngOnInit(): void {
  }

  initColumns() {
    this.columns = {
      name: { title: 'Role', type: 'string', filter: false, sort: false },
      
      description_ar: { title: 'description_ar',  type: 'string', filter: false, sort: false },
      description_en: { title: 'description_en',  type: 'string', filter: false, sort: false },
      
      created_at: { title: 'Created At',   type: 'string',  filter: false, sort: false },
      updated_at: { title: 'Updated At',   type: 'string',  filter: false, sort: false },
    }
  }

  onAdd(data) {
    this.router.navigateByUrl('/pages/roles/add');
  }

  onEdit(data) {
    this.router.navigateByUrl(`/pages/roles/edit/${data['id']}`);
  }

  onView(data) {
    this.router.navigateByUrl(`/pages/roles/view/${data['id']}`);
  }

  onDelete(data)
  {
    let acivate = '';
    if (data['active'])
      acivate = 'deactivate';
    else 
      acivate = 'activate'
    console.log(data);
    this.nbDialogService.open(DeleteModalComponent, {
      context: {
        title: 'Are you sure you want to '+ acivate  +' this Role?',
        message: `Roles: ${data['name']}`,
        onDelete: () => {
          this.roleService.updateRoleActiviation(data['_id'],!data['active'])
            .then(response => {
              console.log(response);
              this.dashboardTable.source.refresh();
              this.toastRef == this.toastrService.show('Role has been successfully '+acivate+'d','Delete Role',{
                position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
                limit : 3,
                hasIcon : true,
                icon : 'checkmark-circle-outline',
                destroyByClick: true,
                status: "danger"
              });

            })
            .catch(error => console.error(error));
        }
      }
    });
  }


}
