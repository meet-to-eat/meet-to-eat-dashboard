import { Injectable } from '@angular/core';
import { HttpWrapperService } from '../../../services/http-wrapper.service';
import { environment } from '../../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpWrapperService) { }

  getAllRoles({ params }) {
    return  this.http.get({ url : `${environment.API_URL}/roles`,params});
  }

  getRoleById(roleId ){
    console.log(roleId)
    return this.http.get({
      url: `${environment.API_URL}/role/${roleId}`,
  });
  }

  addNewRole(RoleData:any){
    return this.http.post({
      url: `${environment.API_URL}/role`,
      body: RoleData,
  });
  }

  updateRole(roleId:string,roleData:any){
    console.log(roleData);
    return this.http.put({
      url: `${environment.API_URL}/roles/${roleId}`,
      body: roleData,
      headers: {},
      params: {}
  })
  }

  updateRoleActiviation(roleId:string,active:boolean){
    return this.http.put({
      url: `${environment.API_URL}/roles/${roleId}/active`,
      body: {active: active }
  })
  }

  addPermissionsToRole(roleId:string,permissionData:any){
    return this.http.put({
      url: `${environment.API_URL}/roles/${roleId}/add-permissions`,
      body:  permissionData ,
      headers: {},
      params: {}
  })
  }

}
