import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddRoleComponent } from './add-role/add-role.component';
import { ViewRolesComponent } from './view-roles/view-roles.component';
import { Routes, RouterModule } from '@angular/router';
import { RolesService } from './roles.service';
import { SharedModule } from '../../../shared/shared.module';
import { ViewRoleComponent } from './view-role/view-role.component';
import { EditRolesComponent } from './edit-roles/edit-roles.component';
import { GeneralPermissionsGuard } from '../../../services/general-permissions-guard.service';
import { ROLE_READ, ROLE_CREATE, ROLE_READ_ONE, ROLE_UPDATE } from '../../../utils/permissions.constants';

const routes: Routes = [
  {
    path: '',
    component: ViewRolesComponent,
    // canActivate: [GeneralPermissionsGuard],
    // data: {
      // permissions: [ROLE_READ]
    // }
  }, 
  {
    path: 'add',
    component: AddRoleComponent,
    // canActivate: [GeneralPermissionsGuard],
    // data: {
      // permissions: [ROLE_CREATE]
    // }
  },
  {
    path: 'view/:roleId',
    component: ViewRoleComponent,
    // canActivate: [GeneralPermissionsGuard],
    // data: {
      // permissions: [ROLE_READ_ONE]
    // }
  },
  {
    path: 'edit/:roleId',
    component: EditRolesComponent,
    // canActivate: [GeneralPermissionsGuard],
    // data: {
      // permissions: [ROLE_UPDATE]
    // }
  },
];

@NgModule({
  declarations: [AddRoleComponent, ViewRolesComponent, ViewRoleComponent,EditRolesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  providers: [
    RolesService
  ]
})

export class RolesModule {

}
