import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { RolesService } from '../roles.service';
import { PermissionsService } from '../../permissions/permissions.service';
import { BehaviorSubject } from 'rxjs';
import { DashboardTableComponent } from '../../../../shared/dashboard-table/dashboard-table.component';
import { environment } from '../../../../../environments/environment';
import { BooleanCellComponent } from '../../../../shared/boolean-cell/boolean-cell.component';
import { DateCellComponent } from '../../../../shared/date-cell/date-cell.component';

@Component({
  selector: 'ngx-view-role',
  templateUrl: './view-role.component.html',
  styleUrls: ['./view-role.component.scss']
})
export class ViewRoleComponent implements OnInit { 

  endpoint: string;
  columns: any = undefined;
  roles: BehaviorSubject<any[]> = new BehaviorSubject([]);

  @ViewChild('dashboardTable', { static: false }) dashboardTable: DashboardTableComponent;

  roleId: string;
  role:any;
  permissionsForRole: any[];
  constructor(
    private activateRoute:ActivatedRoute
    , private roleService:RolesService
    , private permissionService:PermissionsService) { 
      this.initColumns();
    }

  ngOnInit(): void {
    this.roleId = this.activateRoute.snapshot.paramMap.get("roleId");
    console.log('roleId : ',this.roleId)
    this.endpoint = `${environment.API_URL}/role/${this.roleId}`;
    this.roleService.getRoleById(this.roleId).then((response:any) => {
      console.log(response);
      this.role = response;
    });
  }

  initColumns() {
    this.columns = {
      name: { title: 'Permission', type: 'string', filter: false, sort: false },
    }
  }
}
