import { Component, OnInit, ViewChild } from '@angular/core';
import { TextBoxFormItem } from '../../../../shared/dynamic-form/form-items/text-box';
import { Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { RolesService } from './../roles.service';
// import { PermissionsService } from './../../permissions/permissions.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { DashboardTableComponent } from '../../../../shared/dashboard-table/dashboard-table.component';
import { environment } from '../../../../../environments/environment';
import { DateCellComponent } from '../../../../shared/date-cell/date-cell.component';
import { BooleanCellComponent } from '../../../../shared/boolean-cell/boolean-cell.component';
import { NbGlobalPhysicalPosition, NbToastRef, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-edit-roles',
  templateUrl: './edit-roles.component.html',
  styleUrls: ['./edit-roles.component.scss']
})
export class EditRolesComponent implements OnInit {
  
  @ViewChild('dashboardTable', { static: false }) dashboardTable: DashboardTableComponent;

  formItems: BehaviorSubject<any[]> = new BehaviorSubject([]);
  endpoint: string;
  actions: any[];
  columns: any = undefined;
  roleId:string;
  role:any;
  permissionList = [];
  errorMessage:any[] = [];
  toastRef: NbToastRef;
  
  constructor(
    private roleService:RolesService,
    private router:Router,
    private activateRoute:ActivatedRoute ,
    private toasterService: NbToastrService
    ) {
      this.endpoint = `${environment.API_URL}/permissions`;
      this.columns = {
        name: { title: 'Permission', type: 'string'},
      }
     }

  ngOnInit(): void {
    this.roleId = this.activateRoute.snapshot.paramMap.get("roleId");
    this.roleService.getRoleById(this.roleId).then((response) => {
      //Form
      this.role = response['data'];
      this.buildForm(response['data']);
   
      //Table
      this.permissionList = [];
      for (let per of this.role['permissions']){
        this.permissionList.push(per['_id']);
      }
    });
  }

  buildForm(data){
    const name = new TextBoxFormItem({
      key: 'name',
      type: 'text',
      value: data['name'],
      placeholder: 'Role Name',
      showedLabel: 'Role Name',
      validators: [Validators.required]
    });
    this.formItems.next([name]);
  }

  onSubmit(data){
    const RoleData = Object.assign({ name: data['name']} );
    this.roleService.updateRole(this.roleId,RoleData).then((val) => {
      this.router.navigate(['pages/roles']);
      this.toastRef =  this.toasterService.show('Role has been Edited successfully','Edit Role',{
        status : "info",
        icon : "checkmark-circle-2-outline",
        position :NbGlobalPhysicalPosition.BOTTOM_RIGHT,
        hasIcon :true,
      });
    }).catch(Error => {
      console.error(Error);
      this.errorMessage = Error['error']['message'];
    });
  }

  onSubmitCheckList(data : any[]){    
    const roleData = { name: this.role['name'],permissions : data };
    console.log(roleData);
    this.roleService.updateRole(this.roleId,roleData).then((val) => {
      this.router.navigate(['pages/roles']);
      this.toastRef =  this.toasterService.show('Role has been Edited successfully','Edit Role',{
        status : "info",
        icon : "checkmark-circle-2-outline",
        position :NbGlobalPhysicalPosition.BOTTOM_RIGHT,
        hasIcon :true,
      });
    })
  }

}
