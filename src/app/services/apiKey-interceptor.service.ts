import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NbLayoutDirectionService } from '@nebular/theme';
import { map, switchMap, take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiKeyInterceptorService implements HttpInterceptor {


    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {

        const auth_app_token = JSON.parse(localStorage.getItem('auth_app_token'));



        if (auth_app_token && auth_app_token.apiKey) {

            return next.handle(req.clone({
                headers:
                    new HttpHeaders({
                        'Authorization': ' Bearer ' + auth_app_token.apiKey,
                    }),
            }));
        }

        return next.handle(req);


    }


}

