import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JsonPipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})

export class HttpWrapperService {

  constructor(private http: HttpClient) { }

  get({ url = '', headers = {}, params = {} }) {
    if (!url) return Promise.reject('no url provided');

    return this.http.get(url, { headers, params }).toPromise();
  }


  post({
    url = '',
    body = {},
    headers = {},
    params = {}
  }) {
    if (!url) return Promise.reject('no url provided');

    return this.http.post(url, body, { headers, params })
      .toPromise();

  }

  put({
    url = '',
    body = {},
    headers = {},
    params = {}
  }) {
    if (!url) return Promise.reject('no url provided');

    return this.http.put(url, body, {  headers, params })
      .toPromise();
  }


  delete({
    url = '',
    params = {}
  }) {

    if (!url) return Promise.reject('no url provided');

    return this.http.delete(url, { params })
      .toPromise();
  }

}
