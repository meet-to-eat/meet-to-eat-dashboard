import { RoleProviderService } from "./role-provider.service";
import { CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {Location} from '@angular/common';

@Injectable({
  providedIn: "root",
})
export class GeneralPermissionsGuard implements CanActivate {
  constructor(private roleProviderService: RoleProviderService,private location: Location) {}

  canActivate(routeSnapshot: ActivatedRouteSnapshot) {
    if(this.roleProviderService.isGranted(
      routeSnapshot.data["permissions"],
      routeSnapshot.data ? routeSnapshot.data["operator"] : "and"
    ))
    return true;

    else
      this.location.back();

  }
}
