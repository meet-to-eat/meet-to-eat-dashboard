import { BehaviorSubject, of } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: "root",
})
export class AuthService {
  onUserChanges: BehaviorSubject<any> = new BehaviorSubject(null);
  constructor(
    private httpClient: HttpClient,
    private router: Router,
  ) {}

  login(value) {
    this.httpClient
      .post(`${environment.API_URL}/login`, {
        ...value,
      }).pipe(catchError((error) => {console.log(error); return of([])}))
      .subscribe((res: any) => {
        console.log(res);
        if (res.access_token) {
          localStorage.setItem(
            "auth_app_token",
            JSON.stringify({ apiKey: res.access_token })
          );
              let acl = {};
              if (res && res.permissions) {
                acl['role'] = res.permissions.map(
                  (x) => x.name
                );
                localStorage.setItem("role", 'role');
                this.onUserChanges.next({apiKey : res.apiKey,userRole: 'role'})
              }
              localStorage.setItem("ACL", JSON.stringify(acl));
              this.router.navigateByUrl('');
            }
      })
  }
}
