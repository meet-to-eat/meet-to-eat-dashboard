import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NbLayoutDirectionService } from '@nebular/theme';
import { map, switchMap, take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LanguageInterceptorService implements HttpInterceptor {

    language: Observable<string>;

    constructor(
        private directionService: NbLayoutDirectionService
    ) {
        this.language =
            this.directionService.onDirectionChange()
                .pipe(
                    map(direction => direction == 'ltr' ? 'en' : 'ar')
                );

    }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {


        let lang = 'en';
        this.language
            .pipe(take(1))
            .subscribe(currentLang => {
                lang = currentLang;
            });


        let IsDescParam = this.getIsDescParamValue(req.urlWithParams);

        if (IsDescParam) {

            let IsDesc = IsDescParam == 'DESC' ? true : false;

            console.warn(req.urlWithParams);
            console.warn(req.urlWithParams.replace(/IsDesc=\w*/g, `IsDesc=${String(IsDesc)}`));

            let newHttpRequest = new HttpRequest(
                req.method,
                req.urlWithParams.replace(/IsDesc=\w*/g, `IsDesc=${String(IsDesc)}`),
                {
                    headers: new HttpHeaders({ 'Accept-Language': lang }),

                });

            return next.handle(newHttpRequest);
        }

        return next.handle(req.clone({ headers: new HttpHeaders({ 'Accept-Language': lang }) }));

    }

    getIsDescParamValue(url) {
        let splittedUrl = url.split('&');

        for (let value of splittedUrl)
            if (value.includes('IsDesc'))
                return value.replace('IsDesc=', '')

        return null;
    }
}

