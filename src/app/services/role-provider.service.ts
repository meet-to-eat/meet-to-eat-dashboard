import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleProviderService {

  constructor(private authService: AuthService,private router:Router) { 
  }
  getRole(){
    if(localStorage.getItem('role')){
      console.log(localStorage.getItem('role'));
      return of(localStorage.getItem('role'));
    }
    else{
      this.router.navigateByUrl("/auth/login");
    } 
  }
  isGranted(permissions : string[],operator = 'and'){
    let rol_acl = JSON.parse(localStorage.getItem('ACL'))[localStorage.getItem('role')]
    if(operator == 'and')
    return permissions.every((el)=> rol_acl.includes(el));
    else{
      let canAccess : boolean = false
      permissions.forEach((value) => {if (rol_acl.includes(value)) {canAccess = true;}})
      return of(canAccess);
    }
  
  }
}
