import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class IsAuthenticatedService implements CanActivate {

  constructor(
    private router: Router
  ) { }

  canActivate() {
	return true;
/* const auth_app_token = JSON.parse(localStorage.getItem('auth_app_token'));
  
  if (auth_app_token && auth_app_token.apiKey 
    //TODO: uncomment this when finish the dashboard
      // && auth_app_token['2fa_result']
      )
      return true;

    this.router.navigate(['/auth/login']);
  */
  }
}
