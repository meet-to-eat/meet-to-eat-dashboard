import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
// import { AnalyticsService } from './@core/utils/analytics.service';
// import { SeoService } from './@core/utils/seo.service';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(
    // private analytics: AnalyticsService, 
    // private seoService: SeoService,
    private router : Router

    ) {
  }

  ngOnInit(): void {
    let acl = localStorage.getItem('ACL')
    let role = localStorage.getItem('role')
    if(!acl || !role)
    this.router.navigateByUrl('/auth/login');

    // this.analytics.trackPageViews();
    // this.seoService.trackCanonicalChanges();
  }
}
