import * as moment from 'moment';

export const utcToDate = (utcDate) => {
	try{
	
		const localTimeString = moment(utcDate).local().format('YYYY-MM-DD HH:mm:ss');

        return localTimeString;
	
	}catch(error){
		console.error(error)
	}
}

export const dateToUtc = (localDate) => {
	try{

		const utcString = moment.utc(localDate).format();
		return utcString;

	}catch(error){
		console.error(error);
	}
}