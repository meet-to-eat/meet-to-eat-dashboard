import { AuthService } from '../../services/auth.service';
import { map } from "rxjs/operators";
import { environment } from "./../../../environments/environment";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { NbAuthService, NbAuthJWTToken } from "@nebular/auth";

@Component({
  selector: "ngx-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  constructor(
    private authService : AuthService
  ) {}

  loginForm: FormGroup = new FormGroup({
    email: new FormControl("", [Validators.email]),
    password: new FormControl("", [Validators.minLength(4)]),
  });

  ngOnInit(): void {
    const el = document.getElementById("nb-global-spinner");
    if (el) {
      el.style["display"] = "none";
    }
  }

  onSubmit() {
    this.authService.login(this.loginForm.value);
  }
}
