import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { TwoFactorAuthComponent } from './two-factor-auth/two-factor-auth.component';
import { Routes, RouterModule } from '@angular/router';
import { NbButtonModule, NbInputModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
    {
      path: '2fa',
      component: TwoFactorAuthComponent,
    },
    {
      path: 'login',
      component: LoginComponent,
    },

]

@NgModule({
  declarations: [
    LoginComponent, 
    TwoFactorAuthComponent,
    
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NbButtonModule,
    NbInputModule,
  ],
  exports: [

  ]
})
export class CustomAuthModule { }
