/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  // API_URL: 'http://3.19.32.198:3000',
  API_URL: 'http://192.168.43.151/MeetToEat/public/api/Cpanel',
  // API_URL: 'http://3.22.205.87:3000'
};
